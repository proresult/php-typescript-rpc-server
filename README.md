PHP Typescript RPC Server
=========================

This project contains shared code used in the php-typescript-rpc-codegen and in the code that uses php-typescript-rpc-codegen to build a php server and matching typescript client.

Should be considered beta level. Proresult uses it in production, and don't anticipate backwards breaking changes to happen within 0.x versions.

Created by Proresult AS (https://www.proresult.no)

## Deploying new versions

Users with commit access to the master branch can deploy new versions simply by git-tagging the commit with an appropriate version tag (v.0.x.y) and push the tag to bitbucket.

The new version should then be available on packagist, so a composer update to new version in dependent project(s) should work.