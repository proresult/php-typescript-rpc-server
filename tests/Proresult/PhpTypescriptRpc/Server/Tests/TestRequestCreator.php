<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests;

    use Laminas\Diactoros\ServerRequest;
    use Laminas\Diactoros\StreamFactory;
    use Psr\Http\Message\ServerRequestInterface;
    use Psr\Http\Message\StreamInterface;

    class TestRequestCreator {
        public string $baseUri = "http://localhost:8040";

        /**
         * TestRequestCreator constructor.
         *
         * @param string $baseUri
         */
        public function __construct(string $baseUri) {
            $this->baseUri = $baseUri;
        }

        /**
         * @param object|null $body
         *
         * @return StreamInterface|null
         * @throws \JsonException
         */
        private function bodyStream(?object $body): StreamInterface {
            $factory     = new StreamFactory();
            if($body != null) {
                $jsonBodyStr = json_encode($body, JSON_THROW_ON_ERROR);
                return $factory->createStream($jsonBodyStr);
            }
            return $factory->createStream();
        }

        /**
         * @param array<string, string|int|float|bool>  $queryParams
         *
         * @return array<string, string>
         */
        private function queryParamsStringValues(array $queryParams): array {
            $mapper = fn(string|int|float|bool $v) => match(gettype($v)) {
                "string" => $v,
                "integer" => strval($v),
                "double" => strval($v),
                "boolean" => $v ? "true" : "false",
                default => throw new \Exception("Invalid query param value type")
            };
            return array_map($mapper, $queryParams);
        }

        /**
         * @throws \JsonException
         */
        function rpcRequest(string $path, string $method, array $queryParams = [], ?object $body = null): ServerRequestInterface {
            $req = new ServerRequest(
                uri: "{$this->baseUri}{$path}",
                method: $method,
                body: $this->bodyStream($body),
                queryParams: $this->queryParamsStringValues($queryParams),
            );
            $req = $req->withAddedHeader("Content-Type", "application/json");
            return $req;
        }
    }