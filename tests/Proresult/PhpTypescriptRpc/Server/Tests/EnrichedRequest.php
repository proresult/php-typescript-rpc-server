<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Server\Tests;

use Proresult\PhpTypescriptRpc\Server\WrappedRequest;
use Psr\Http\Message\ServerRequestInterface;

class EnrichedRequest extends WrappedRequest {
    public const ATTR_REQ_ID = "EnrichedRequest.REQ_ID";
    public const HEADER_VAL_AUTH = "SuperSecret";
    public const HEADER_KEY_AUTH = "X-Auth";
    public const HEADER_KEY_REQUEST_ID = "X-Request-Id";

    private bool $isAuthenticated;

    public static function requestIdFromHeader(ServerRequestInterface $request): int {
        foreach($request->getHeader(self::HEADER_KEY_REQUEST_ID) as $header) {
            $id = intval($header);
            if($id > 0) {
                return $id;
            }
        }
        return 0;
    }

    public function __construct(ServerRequestInterface $request) {
        $enriched = $request->withAttribute(self::ATTR_REQ_ID, self::requestIdFromHeader($request));
        $this->isAuthenticated = $enriched->getHeaderLine(self::HEADER_KEY_AUTH) === self::HEADER_VAL_AUTH;
        parent::__construct($enriched);
    }

    public function getRequestId(): int {
        return $this->getAttribute(self::ATTR_REQ_ID, 0);
    }

    public function isAuthenticated(): bool {
        return $this->isAuthenticated;
    }
}