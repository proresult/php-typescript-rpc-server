<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Server\Tests;

use Throwable;

class TestMonitoringImplementation extends \Proresult\PhpTypescriptRpc\Server\DefaultMonitoringImplementation {

    /** @var array<int, Throwable> */
    public array $thrown;

    public int $mode;

    /**
     * @param int $mode 0 = throw exceptions when reported. 1 = Don't throw exceptions reported
     */
    public function __construct(int $mode = 0) {
        $this->mode = $mode;
    }

    public function getLastThrown(): ?Throwable {
        $ret = end($this->thrown);
        if ($ret !== false) {
            return $ret;
        } else {
            return null;
        }
    }

    /**
     * @throws Throwable
     */
    protected function logThrowable(int $priority, Throwable $t): void {
        $this->thrown[] = $t;
        if ($this->mode === 0) {
            throw $t;
        }
    }
}