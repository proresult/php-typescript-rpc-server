<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests;

    use Laminas\Diactoros\Response;
    use Proresult\PhpTypescriptRpc\Server\ResponseFactoryInterface;
    use Psr\Http\Message\ResponseInterface;

    class TestResponseFactory implements ResponseFactoryInterface {

        public function newResponse(int $statusCode, ?string $body, ?string $contentType) : ResponseInterface {
            $response = new Response(status: $statusCode);
            if($body != null) {
                $response->getBody()->write($body);
                $response->getBody()->rewind();
            }
            if($contentType != null) {
                $response = $response->withHeader("Content-Type", $contentType);
            }
            return $response;
        }
    }