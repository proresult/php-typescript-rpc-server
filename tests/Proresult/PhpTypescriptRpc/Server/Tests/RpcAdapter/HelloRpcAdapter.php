<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests\RpcAdapter;


    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
    use Proresult\PhpTypescriptRpc\Server\Tests\Models\HelloRequest;
    use Proresult\PhpTypescriptRpc\Server\Tests\Rpc\HelloRpc;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;

    class HelloRpcAdapter implements RequestResponseInterface {

        private RequestResponseUtils $requestResponseUtils;
        private ?HelloRpc            $rpc;

        /**
         * RequestResponseAdapter constructor.
         */
        public function __construct(RequestResponseUtils $adapterUtils, ?HelloRpc $rpc = null) {
            $this->requestResponseUtils = $adapterUtils;
            $this->rpc                  = $rpc;
        }

        public function hello(ServerRequestInterface $request): ResponseInterface {
            $this->requestResponseUtils->ensureMethod($request, "POST");
            $requestBodyStr = $this->requestResponseUtils->getBodyStr($request);
            $rpcRequest = $this->requestResponseUtils->deserialize($requestBodyStr, HelloRequest::class);
            if($this->rpc === null) {
                $this->rpc = HelloRpc::initFromRequest($request);
            }
            $rpcResponse = $this->rpc->hello($rpcRequest);

            return $this->requestResponseUtils->successfulRpcJsonResponse($rpcResponse);
        }

        public function failingHello(ServerRequestInterface $request): ResponseInterface {
            $this->requestResponseUtils->ensureMethod($request, "POST");
            $requestBodyStr = $this->requestResponseUtils->getBodyStr($request);
            $rpcRequest = $this->requestResponseUtils->deserialize($requestBodyStr, HelloRequest::class);
            if($this->rpc === null) {
                $this->rpc = HelloRpc::initFromRequest($request);
            }
            $rpcResponse = $this->rpc->failingHello($rpcRequest);
            return $this->requestResponseUtils->successfulRpcJsonResponse($rpcResponse);
        }

        public function unauthorizedHello(ServerRequestInterface $request): ResponseInterface {
            $this->requestResponseUtils->ensureMethod($request, "POST");
            $requestBodyStr = $this->requestResponseUtils->getBodyStr($request);
            $rpcRequest = $this->requestResponseUtils->deserialize($requestBodyStr, HelloRequest::class);
            if($this->rpc === null) {
                $this->rpc = HelloRpc::initFromRequest($request);
            }
            $rpcResponse = $this->rpc->unauthorizedHello($rpcRequest);
            return $this->requestResponseUtils->successfulRpcJsonResponse($rpcResponse);
        }

        public function handleRequest(ServerRequestInterface $request): ?ResponseInterface {
            return match($request->getUri()->getPath()) {
                "/HelloRpc/hello" => $this->hello($request),
                "/HelloRpc/failingHello" => $this->failingHello($request),
                "/HelloRpc/unauthorizedHello" => $this->unauthorizedHello($request),
                default => null,
            };
        }

    }