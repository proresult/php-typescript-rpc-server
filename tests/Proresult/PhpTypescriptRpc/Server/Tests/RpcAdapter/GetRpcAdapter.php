<?php
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests\RpcAdapter;

    use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\Tests\Rpc\GetRpc;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;

    class GetRpcAdapter implements RequestResponseInterface {
        private RequestResponseUtils    $requestResponseUtils;
        private ?GetRpc                 $rpc;

        /**
         * @param RequestResponseUtils $requestResponseUtils
         * @param GetRpc|null          $rpc
         */
        public function __construct(RequestResponseUtils $requestResponseUtils, ?GetRpc $rpc = null) {
            $this->requestResponseUtils = $requestResponseUtils;
            $this->rpc                  = $rpc;
        }

        public function getSome(ServerRequestInterface $request): ResponseInterface {
            $this->requestResponseUtils->ensureMethod($request, "GET");
            if ($this->rpc === null) {
                $this->rpc = GetRpc::initFromRequest($request);
            }
            $params = $this->requestResponseUtils->getQueryParams($request);
            $txt1 = $this->requestResponseUtils->getStringParam($params, "txt1", null);
            $num1 = $this->requestResponseUtils->getIntParam($params, "num1", null);
            $txt2 = $this->requestResponseUtils->getStringParam($params, "txt2", "defaultValue");
            $rpcResponse = $this->rpc->getSome(txt1: $txt1, num1: $num1, txt2: $txt2);
            return $this->requestResponseUtils->successfulRpcJsonResponse($rpcResponse);
        }

        public function failingGet(ServerRequestInterface $request): ResponseInterface {
            $this->requestResponseUtils->ensureMethod($request, "GET");
            $params = $this->requestResponseUtils->getQueryParams($request);
            $fl1 = $this->requestResponseUtils->getFloatParam($params, "fl1", null);
            if($this->rpc === null) {
                $this->rpc = GetRpc::initFromRequest($request);
            }
            $rpcResponse = $this->rpc->failingGet(fl1: $fl1);
            return $this->requestResponseUtils->successfulRpcJsonResponse($rpcResponse);
        }

        public function withNullableParam(ServerRequestInterface $request): ResponseInterface {
            $this->requestResponseUtils->ensureMethod($request, "GET");
            $params = $this->requestResponseUtils->getQueryParams($request, "GET");
            $txtOrNull = $this->requestResponseUtils->getOptionalStringParam($params, "txtOrNull", null);
            $intOrNull = $this->requestResponseUtils->getOptionalIntParam($params, "intOrNull", null);
            if($this->rpc === null) {
                $this->rpc = GetRpc::initFromRequest($request);
            }
            $rpcResponse = $this->rpc->withNullableParam($txtOrNull, $intOrNull);
            return $this->requestResponseUtils->successfulRpcJsonResponse($rpcResponse);
        }


        public function handleRequest(ServerRequestInterface $request) : ?ResponseInterface {
            return match($request->getUri()->getPath()) {
                "/GetRpc/getSome" => $this->getSome($request),
                "/GetRpc/failingGet" => $this->failingGet($request),
                "/GetRpc/withNullableParam" => $this->withNullableParam($request),
                default => null,
            };
        }
    }