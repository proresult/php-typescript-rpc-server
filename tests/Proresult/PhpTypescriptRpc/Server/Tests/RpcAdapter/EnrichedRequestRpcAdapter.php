<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Server\Tests\RpcAdapter;

use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
use Proresult\PhpTypescriptRpc\Server\Tests\EnrichedRequest;
use Proresult\PhpTypescriptRpc\Server\Tests\Rpc\EnrichedRequestRpc;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class EnrichedRequestRpcAdapter implements RequestResponseInterface {
    private RequestResponseUtils $requestResponseUtils;
    private ?EnrichedRequestRpc $rpc;

    public function __construct(RequestResponseUtils $requestResponseUtils, ?EnrichedRequestRpc $rpc = null) {
        $this->requestResponseUtils = $requestResponseUtils;
        $this->rpc = $rpc;
    }

    public function getSome(ServerRequestInterface $request): ResponseInterface {
        $this->requestResponseUtils->ensureMethod($request, "GET");
        if ($this->rpc === null) {
            $request = new EnrichedRequest($request);
            $this->rpc = EnrichedRequestRpc::initFromRequest($request);
        }
        $params = $this->requestResponseUtils->getQueryParams($request);
        $a = $this->requestResponseUtils->getStringParam($params, "a", null);
        $rpcResponse = $this->rpc->getSome(a: $a);
        return $this->requestResponseUtils->successfulRpcJsonResponse($rpcResponse);
    }

    public function handleRequest(ServerRequestInterface $request): ?ResponseInterface {
        return match ($request->getUri()->getPath()) {
            "/EnrichedRequestRpc/getSome" => $this->getSome($request),
            default => null,
        };
    }
}