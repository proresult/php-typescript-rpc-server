<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Server\Tests\RpcAdapter;

use Proresult\PhpTypescriptRpc\Server\Tests\AuthController1;
use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
use Proresult\PhpTypescriptRpc\Server\Tests\EnrichedRequest;
use Proresult\PhpTypescriptRpc\Server\Tests\Rpc\AuthorizedRpc;
use Proresult\PhpTypescriptRpc\Server\Tests\Rpc\EnrichedRequestRpc;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AuthorizedRpcAdapter implements RequestResponseInterface {
    private RequestResponseUtils $requestResponseUtils;
    private ?AuthorizedRpc $rpc;

    public function __construct(RequestResponseUtils $requestResponseUtils, ?EnrichedRequestRpc $rpc = null) {
        $this->requestResponseUtils = $requestResponseUtils;
        $this->rpc = $rpc;
    }

    public function getSome(ServerRequestInterface $_httpRequest): ResponseInterface {
        $this->requestResponseUtils->ensureMethod($_httpRequest, "GET");
        $_httpRequest = new EnrichedRequest($_httpRequest);
        $authController = new AuthController1($_httpRequest); // Authentication control happens here, throws if not authenticated
        $params = $this->requestResponseUtils->getQueryParams($_httpRequest);
        $a = $this->requestResponseUtils->getStringParam($params, "a", null);
        if (!$authController->allow()) { // Default auth check method
            throw new ForbiddenException();
        }
        if ($this->rpc === null) {
            $this->rpc = AuthorizedRpc::initFromRequest($_httpRequest);
        }
        $rpcResponse = $this->rpc->getSome(a: $a);
        return $this->requestResponseUtils->successfulRpcJsonResponse($rpcResponse);
    }

    public function specialAuthorizedSome(ServerRequestInterface $_httpRequest): ResponseInterface {
        $this->requestResponseUtils->ensureMethod($_httpRequest, "GET");
        $_httpRequest = new EnrichedRequest($_httpRequest);
        $_authController = new AuthController1($_httpRequest); // Authentication control happens here, throws if not authenticated
        $params = $this->requestResponseUtils->getQueryParams($_httpRequest);
        $what = $this->requestResponseUtils->getIntParam($params, "what", null);
        if (!$_authController->specialAuthorizedSome($what)) { // Default auth check method
            throw new ForbiddenException();
        }
        if ($this->rpc === null) {
            $this->rpc = AuthorizedRpc::initFromRequest($_httpRequest);
        }
        $_rpcResponse = $this->rpc->specialAuthorizedSome(what: $what);
        return $this->requestResponseUtils->successfulRpcJsonResponse($_rpcResponse);
    }

    public function handleRequest(ServerRequestInterface $request): ?ResponseInterface {
        return match ($request->getUri()->getPath()) {
            "/AuthorizedRpc/getSome" => $this->getSome($request),
            "/AuthorizedRpc/specialAuthorizedSome" => $this->specialAuthorizedSome($request),
            default => null,
        };
    }
}