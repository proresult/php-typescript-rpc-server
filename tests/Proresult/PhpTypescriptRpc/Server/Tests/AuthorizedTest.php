<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Server\Tests;

use PHPUnit\Framework\TestCase;
use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;
use Proresult\PhpTypescriptRpc\Server\Http;
use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
use Proresult\PhpTypescriptRpc\Server\RpcRouter;
use Proresult\PhpTypescriptRpc\Server\Tests\Models\GetSomeResponse;
use Proresult\PhpTypescriptRpc\Server\Tests\RpcAdapter\AuthorizedRpcAdapter;

class AuthorizedTest extends TestCase {
    private TestRequestCreator $requestCreator;
    private RequestResponseUtils $requestResponseUtils;

    public function __construct() {
        $this->requestCreator = new TestRequestCreator("http://localhost:8040");
        $this->requestResponseUtils = new RequestResponseUtils(new TestResponseFactory());
        parent::__construct();
    }

    function testGetSomeUnauthenticated() {
        $params = ["a" => "a"];
        $request = $this->requestCreator->rpcRequest("/rpc/AuthorizedRpc/getSome", "GET", $params);
        $rpcAdapter = new AuthorizedRpcAdapter($this->requestResponseUtils);
        $handler = new RpcRouter(new TestResponseFactory(), "/rpc", $rpcAdapter);
        $response = $handler->process($request);
        $this->assertEquals(Http::STATUS_CODE_UNAUTHORIZED, $response->getStatusCode());
        $someRpcException = TestHelper::someRpcExceptionFromResponse($request, $response);
        $this->assertEquals(0, $someRpcException->getCode());
        $this->assertEquals(UnauthorizedException::DEFAULT_MESSAGE, $someRpcException->getMessage());
    }

    function testGetSomeAuthenticated() {
        $params = ["a" => "a"];
        $request = $this->requestCreator->rpcRequest("/rpc/AuthorizedRpc/getSome", "GET", $params)
            ->withHeader(EnrichedRequest::HEADER_KEY_REQUEST_ID, "5588")
            ->withHeader(EnrichedRequest::HEADER_KEY_AUTH, EnrichedRequest::HEADER_VAL_AUTH);
        $rpcAdapter = new AuthorizedRpcAdapter($this->requestResponseUtils);
        $handler = new RpcRouter(new TestResponseFactory(), "/rpc", $rpcAdapter);
        $response = $handler->process($request);
        $this->assertEquals(Http::STATUS_CODE_OK, $response->getStatusCode());
        $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
        $getSomeResponse = $this->requestResponseUtils->deserialize($responseBody, GetSomeResponse::class);
        /** @var GetSomeResponse $getSomeResponse */
        $this->assertEquals(5588,  $getSomeResponse->num1);
        $this->assertEquals("a",  $getSomeResponse->txt1);
    }

    function testSpecialAuthorizedSome() {
        // First do a call that fails authorization check
        $params = ["what" => 3];
        $request = $this->requestCreator->rpcRequest("/rpc/AuthorizedRpc/specialAuthorizedSome", "GET", $params)
            ->withHeader(EnrichedRequest::HEADER_KEY_REQUEST_ID, "5588")
            ->withHeader(EnrichedRequest::HEADER_KEY_AUTH, EnrichedRequest::HEADER_VAL_AUTH);
        $rpcAdapter = new AuthorizedRpcAdapter($this->requestResponseUtils);
        $handler = new RpcRouter(new TestResponseFactory(), "/rpc", $rpcAdapter);
        $response = $handler->process($request);
        $this->assertEquals(Http::STATUS_CODE_FORBIDDEN, $response->getStatusCode());
        $someRpcException = TestHelper::someRpcExceptionFromResponse($request, $response);
        $this->assertEquals(0, $someRpcException->getCode());
        $this->assertEquals(ForbiddenException::DEFAULT_MESSAGE, $someRpcException->getMessage());
        // Do a call that passes authorization check
        $params = ["what" => 300];
        $request = $this->requestCreator->rpcRequest("/rpc/AuthorizedRpc/specialAuthorizedSome", "GET", $params)
            ->withHeader(EnrichedRequest::HEADER_KEY_REQUEST_ID, "5588")
            ->withHeader(EnrichedRequest::HEADER_KEY_AUTH, EnrichedRequest::HEADER_VAL_AUTH);
        $response = $handler->process($request);
        $this->assertEquals(Http::STATUS_CODE_OK, $response->getStatusCode());
        $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
        $getSomeResponse = $this->requestResponseUtils->deserialize($responseBody, GetSomeResponse::class);
        /** @var GetSomeResponse $getSomeResponse */
        $this->assertEquals($params["what"],  $getSomeResponse->num1);
        $this->assertEquals("aaa",  $getSomeResponse->txt1);
    }
}