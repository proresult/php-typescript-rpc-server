<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Server\Tests;

use PHPUnit\Framework\TestCase;
use Proresult\PhpTypescriptRpc\Server\AllowedCorsOrigin;

class AllowedCorsOriginTest extends TestCase {

    public function invalidConstructorArgs(): array {
        return [
            ["", "proresult.app", 8080],
            ["https://", "proresult.app", 8080],
            ["https", "app", 8080, true],
            ["https", "proresult.app", -8080],
            ["https", ".proresult.app", 8080],
        ];
    }

    public function validConstructorArgs(): array {
        return [
            ["http", "proresult.app", 443],
            ["http", "proresult.app", 443, true],
            ["https", "proresult.app", 443],
            ["https", "proresult.app", 443, true],
            ["https", "staging.proresult.app", 443],
            ["https", "v2.rpc.proresult.app", 443, false],
            ["http", "localhost", 8045],
        ];
    }

    /**
     * @dataProvider invalidConstructorArgs
     */
    public function testInvalidInput(string $protocol, string $host, int $port, bool $includingSubDomains = false) {
        $this->expectException(\InvalidArgumentException::class);
        $invalid = new AllowedCorsOrigin($protocol, $host, $port, $includingSubDomains);
    }

    /**
     * @dataProvider validConstructorArgs
     */
    public function testValidConstructorArgs(string $protocol, string $host, int $port, bool $includingSubDomains = false) {
        $valid = new AllowedCorsOrigin($protocol, $host, $port, $includingSubDomains);
        $this->assertEquals($protocol, $valid->protocol);
        $this->assertEquals($host, $valid->host);
        $this->assertEquals($port, $valid->port);
        $this->assertEquals($includingSubDomains, $valid->includingSubDomains);
    }

    public function matchArguments(): array {
        return [
            [new AllowedCorsOrigin("http", "localhost", 8080), "http://localhost:8080", true],
            [new AllowedCorsOrigin("http", "localhost", 8081), "http://localhost:8080", false],
            [new AllowedCorsOrigin("http", "localhost", 8080), "http://v2.localhost:8080", false],
            [new AllowedCorsOrigin("http", "proresult.app", 8080, true), "http://v2.localhost:8080", false],
            [new AllowedCorsOrigin("http", "proresult.app", 8080, true), "http://v2.proresult.app:8080", true],
            [new AllowedCorsOrigin("http", "proresult.app", 8080, true), "https://v2.proresult.app:8080", false],
            [new AllowedCorsOrigin("http", "proresult.app", 8080, true), "https://y-proresult.app:8080", false],
            [new AllowedCorsOrigin("http", "xproresult.app", 8080, true), "https://proresult.app:8080", false],
            [new AllowedCorsOrigin("https", "proresult.app", 443, true), "https://staging.proresult.app", true],
            [new AllowedCorsOrigin("https", "proresult.app", 443, true), "https://proresult.app", true],
            [new AllowedCorsOrigin("https", "proresult.app", 443, false), "https://proresult.app", true],
        ];
    }

    /**
     * @dataProvider matchArguments
     * @param AllowedCorsOrigin $aco
     * @param string            $testOrigin
     * @param bool              $expectMatch
     *
     * @return void
     */
    public function testMatch(AllowedCorsOrigin $aco, string $testOrigin, bool $expectMatch) {
        if ($expectMatch) {
            $this->assertTrue($aco->match($testOrigin));
        } else {
            $this->assertFalse($aco->match($testOrigin));
        }
    }
}