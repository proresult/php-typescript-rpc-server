<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests;

    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Server\AllowedCorsOrigin;
    use Proresult\PhpTypescriptRpc\Server\CorsProcessor;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\BadRequestException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
    use Proresult\PhpTypescriptRpc\Server\Http;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;
    use Proresult\PhpTypescriptRpc\Server\RpcRouter;
    use Proresult\PhpTypescriptRpc\Server\Tests\Models\HelloRequest;
    use Proresult\PhpTypescriptRpc\Server\Tests\Models\HelloResponse;
    use Proresult\PhpTypescriptRpc\Server\Tests\Models\Person;
    use Proresult\PhpTypescriptRpc\Server\Tests\Rpc\HelloRpc;
    use Proresult\PhpTypescriptRpc\Server\Tests\RpcAdapter\HelloRpcAdapter;

    class HelloTest extends TestCase {
        private TestRequestCreator $requestCreator;

        private Person $to, $from;
        private HelloRequest $helloRequest;

        /**
         * HelloTest constructor.
         */
        public function __construct() {
            $this->requestCreator = new TestRequestCreator("http://localhost:8040");
            parent::__construct();
        }


        protected function setUp() : void {
            parent::setUp();
            $this->to = new Person("Test", "Name", 1922);
            $this->from = new Person("Julius", "Cæsar", 1994);

            $this->helloRequest = new HelloRequest();
            $this->helloRequest->to = $this->to;
            $this->helloRequest->from = $this->from;
        }


        function testHelloEndpoint() {
            $startDate = new RpcDateTime(new \DateTimeImmutable());
            $helloEndpoint = new HelloRpc($startDate);
            $response = $helloEndpoint->hello($this->helloRequest);
            $this->assertEquals($startDate, $response->serverStartTime);
            $expectedMessage = "Hello, {$this->to->name()}! Best wishes from {$this->from->name()}.";
            $this->assertEquals($expectedMessage, $response->message);
        }

        // Test doing a hello request with correct CORS setup on the server and request
        function testRequestResponseHelloWithCors() {
            $testResponseFactory = new TestResponseFactory();
            $requestResponseUtils = new RequestResponseUtils($testResponseFactory);
            $helloRequest = $this->helloRequest;
            // Add CORS header to request so that CORS processing works well
            $allowedCorsOrigin = new AllowedCorsOrigin("http", "test.local", 3030);
            $request = $this->requestCreator->rpcRequest("/rpc/HelloRpc/hello", "POST", body: $helloRequest)
                ->withAddedHeader("Origin", $allowedCorsOrigin->originString());

            $startTime = new \DateTimeImmutable();
            $helloRpc = new HelloRpc($startTime);
            $helloRpcAdapter = new HelloRpcAdapter($requestResponseUtils, $helloRpc);
            $handler = new RpcRouter($testResponseFactory, "/rpc", $helloRpcAdapter);
            // Set monitoring interface to get all bad response exceptions thrown
            $handler->setMonitoringInterface(new TestMonitoringImplementation());
            // Setup CORS processing, want to test that too here
            $corsProcessor = new CorsProcessor(["GET", "POST"], [$allowedCorsOrigin], $testResponseFactory, true);
            $handler->setCorsProcessor($corsProcessor);
            $response = $handler->process($request);

            $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
            $helloResponse = $requestResponseUtils->deserialize($responseBody, HelloResponse::class);
            /** @var HelloResponse $helloResponse */

            $expectedResponse = $helloRpc->hello($helloRequest);

            $this->assertEquals($expectedResponse, $helloResponse);
        }

        // Test doing a hello request with CORS setup on the server, but the request not being CORS.
        function testRequestResponseHelloUnusedCorsSetup() {
            $testResponseFactory = new TestResponseFactory();
            $requestResponseUtils = new RequestResponseUtils($testResponseFactory);
            $helloRequest = $this->helloRequest;
            // Add CORS header to request so that CORS processing works well
            $allowedCorsOrigin = new AllowedCorsOrigin("http", "test.local", 3030);
            $request = $this->requestCreator->rpcRequest("/rpc/HelloRpc/hello", "POST", body: $helloRequest);

            $startTime = new \DateTimeImmutable();
            $helloRpc = new HelloRpc($startTime);
            $helloRpcAdapter = new HelloRpcAdapter($requestResponseUtils, $helloRpc);
            $handler = new RpcRouter($testResponseFactory, "/rpc", $helloRpcAdapter);
            // Set monitoring interface to get all bad response exceptions thrown
            $handler->setMonitoringInterface(new TestMonitoringImplementation());
            // Setup CORS processing, want to test that too here
            $corsProcessor = new CorsProcessor(["GET", "POST"], [$allowedCorsOrigin], $testResponseFactory, true);
            $handler->setCorsProcessor($corsProcessor);
            $response = $handler->process($request);

            $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
            $helloResponse = $requestResponseUtils->deserialize($responseBody, HelloResponse::class);
            /** @var HelloResponse $helloResponse */

            $expectedResponse = $helloRpc->hello($helloRequest);

            $this->assertEquals($expectedResponse, $helloResponse);
        }

        // Test that invalid CORS gives bad request exception/response. The origin is here invalid
        function testRequestResponseHelloInvalidCorsRequest() {
            $testResponseFactory = new TestResponseFactory();
            $requestResponseUtils = new RequestResponseUtils($testResponseFactory);
            $helloRequest = $this->helloRequest;
            // Add CORS header to request so that CORS processing works well
            $allowedCorsOrigin = new AllowedCorsOrigin("http", "test.local", 3030);
            $request = $this->requestCreator->rpcRequest("/rpc/HelloRpc/hello", "POST", body: $helloRequest)
                ->withAddedHeader("Origin", "http://invalid.local:3030");

            $startTime = new \DateTimeImmutable();
            $helloRpc = new HelloRpc($startTime);
            $helloRpcAdapter = new HelloRpcAdapter($requestResponseUtils, $helloRpc);
            $handler = new RpcRouter($testResponseFactory, "/rpc", $helloRpcAdapter);
            // Use monitoring that just gathers up any exceptions so that we can check them after
            $monitoring = new TestMonitoringImplementation(1);
            $handler->setMonitoringInterface($monitoring);
            // Setup CORS processing, want to test that too here
            $corsProcessor = new CorsProcessor(["GET", "POST"], [$allowedCorsOrigin], $testResponseFactory, true);
            $handler->setCorsProcessor($corsProcessor);
            $response = $handler->process($request);
            $this->assertEquals('', $response->getHeaderLine("Access-Control-Allow-Origin"));
            $this->assertEquals(Http::STATUS_CODE_BAD_REQUEST, $response->getStatusCode());
            $badRequestException = $monitoring->getLastThrown();
            $this->assertInstanceOf(BadRequestException::class, $badRequestException);
        }

        function testRequestResponseHelloWithoutExplicitConstruction() {
            $testStartTime = new \DateTimeImmutable();
            $helloRequest = $this->helloRequest;
            $request = $this->requestCreator->rpcRequest("/rpc/HelloRpc/hello", "POST", body: $helloRequest);
            $adapterUtils = new RequestResponseUtils(new TestResponseFactory());
            $helloRpcAdapter = new HelloRpcAdapter($adapterUtils);
            $handler = new RpcRouter(new TestResponseFactory(), "/rpc", $helloRpcAdapter);
            $response = $handler->process($request);
            $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
            $helloResponse = $adapterUtils->deserialize($responseBody, HelloResponse::class);
            /** @var HelloResponse $helloResponse */
            $this->assertTrue($helloResponse->serverStartTime >= $testStartTime);
        }

        function testRequestResponseFailingHello() {
            $helloRequest = $this->helloRequest;
            $request = $this->requestCreator->rpcRequest("/rpc/HelloRpc/failingHello", "POST", body: $helloRequest);

            $startTime = new \DateTimeImmutable();
            $helloRpc = new HelloRpc($startTime);
            $requestResponseUtils = new RequestResponseUtils(new TestResponseFactory());
            $helloRpcAdapter = new HelloRpcAdapter($requestResponseUtils, $helloRpc);
            $handler = new RpcRouter(new TestResponseFactory(), "/rpc", $helloRpcAdapter);
            $response = $handler->process($request);
            $this->assertFalse($response->hasHeader("Retry-After"));

            $userfriendlyException = TestHelper::unwrapRpcResponseContainerError($response->getBody()->getContents());
            $this->assertEquals(HelloRpc::failingHelloCode, $userfriendlyException->getCode());
            $this->assertEquals(HelloRpc::failingHelloMessage, $userfriendlyException->getMessage());

            // Try again, with arg that lead to retryable error returned
            $helloRequest->to->lastName = "retry";
            $request = $this->requestCreator->rpcRequest("/rpc/HelloRpc/failingHello", "POST", body: $helloRequest);
            $response = $handler->process($request);
            $this->assertTrue($response->hasHeader("Retry-After"));
            $this->assertEquals("1", $response->getHeaderLine("Retry-After"));
            $userfriendlyException = TestHelper::unwrapRpcResponseContainerError($response->getBody()->getContents());
            $this->assertEquals(HelloRpc::failingHelloCode, $userfriendlyException->getCode());
            $this->assertEquals(HelloRpc::failingHelloMessage, $userfriendlyException->getMessage());
        }

        function testRequestResponseUnauthorizedHello() {
            $helloRequest = $this->helloRequest;
            $request = $this->requestCreator->rpcRequest("/rpc/HelloRpc/unauthorizedHello", "POST", body: $helloRequest);

            $startTime = new \DateTimeImmutable();
            $helloRpc = new HelloRpc($startTime);
            $requestResponseUtils = new RequestResponseUtils(new TestResponseFactory());
            $helloRpcAdapter = new HelloRpcAdapter($requestResponseUtils, $helloRpc);
            $handler = new RpcRouter(new TestResponseFactory(), "/rpc", $helloRpcAdapter);
            $response = $handler->process($request);
            $this->assertEquals(Http::STATUS_CODE_UNAUTHORIZED, $response->getStatusCode());

            $someRpcException = TestHelper::someRpcExceptionFromResponse($request, $response);

            $this->assertEquals(HelloRpc::failingAuthCode, $someRpcException->getCode());
            $this->assertEquals(HelloRpc::failingAuthMessage, $someRpcException->getMessage());
        }

    }