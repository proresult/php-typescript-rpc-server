<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Tests;

use DateTimeZone;
use PHPUnit\Framework\TestCase;
use Proresult\PhpTypescriptRpc\Server\Exceptions\InternalServerException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
use Proresult\PhpTypescriptRpc\Server\Http;
use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;
use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
use Proresult\PhpTypescriptRpc\Server\Exceptions\BadRequestException;
use Proresult\PhpTypescriptRpc\Server\Tests\Models\ComplexEntity;
use Proresult\PhpTypescriptRpc\Server\Tests\Models\HelloRequest;
use Proresult\PhpTypescriptRpc\Server\Tests\Models\HelloResponse;
use Proresult\PhpTypescriptRpc\Server\Tests\Models\Person;
use Proresult\PhpTypescriptRpc\Server\Tests\Models\SubEntity1;

class RequestResponseUtilsTest extends TestCase {
    private RequestResponseUtils $utils;
    public function setUp(): void {

        parent::setUp();
        $this->utils = new RequestResponseUtils(new TestResponseFactory());
    }

    public function testDeserializeISO8601FormattedDate() {
        $utils = new RequestResponseUtils(new TestResponseFactory(), new DateTimeZone("Europe/Oslo"));

        $serialized = '"2023-01-14T23:00:00.000Z"';
        $deserialized = $utils->deserialize($serialized, RpcDateTime::class);

        assert($deserialized instanceof RpcDateTime);

        $expectedTimeZone = new DateTimeZone("Europe/Oslo");
        $actualTimeZone = $deserialized->getTimezone();

        $this->assertEquals($expectedTimeZone->getName(), $actualTimeZone->getName());

        $this->assertEquals("2023-01-15", $deserialized->format("Y-m-d"));
    }

    public function testSerializeDeserializeHappypath() {

        $helloRequest = new HelloRequest();
        $helloRequest->from = new Person("Lasse", "Loff", 1984);
        $helloRequest->to = new Person("Lise", "Limpy", 1981);
        $serialized = $this->utils->serialize($helloRequest);
        $deserialized = $this->utils->deserialize($serialized, HelloRequest::class);
        $this->assertEquals($helloRequest, $deserialized);
    }

    public function testSerializeDeserializeIncorrectType() {

        $helloRequest = new HelloRequest();
        $helloRequest->from = new Person("Lasse", "Loff", 1984);
        $helloRequest->to = new Person("Lise", "Limpy", 1981);
        $serialized = $this->utils->serialize($helloRequest);
        $this->expectException(BadRequestException::class);
        $deserialized = $this->utils->deserialize($serialized, HelloResponse::class);
    }

    public function testSerializeBadRequest() {

        try {
            throw new BadRequestException("Test exception");
        } catch (BadRequestException $bre) {
            $response = $this->utils->badRequestExceptionJsonResponse($bre);
            $responseBodyObj = json_decode($response->getBody()->getContents(), true);
            $this->assertEquals(Http::STATUS_CODE_BAD_REQUEST, $response->getStatusCode());
            $this->assertEquals(["code" => 0, "message" => "Test exception"], $responseBodyObj);
        }
    }

    public function testSerializeUserfriendlyException() {

        try {
            throw new UserfriendlyException("Friendly test exception", 1001);
        } catch (UserfriendlyException $e) {
            $response = $this->utils->userfriendlyExceptionJsonResponse($e);
            $responseBodyObj = json_decode($response->getBody()->getContents(), true);
            $this->assertEquals(Http::STATUS_CODE_OK, $response->getStatusCode());
            $this->assertEquals(["response" => null, "error" => ["code" => 1001, "message" => "Friendly test exception"]], $responseBodyObj);
        }
    }

    public function testSerializeDeserializeComplexEntity() {

        $complex = new ComplexEntity();
        $complex->counter = 123;
        $complex->numOrText = "abc";
        $complex->entity1 = new SubEntity1("sub1");
        $complex->numOrNull = null;
        $complex->numOrTextOrNull = null;
        $complex->maybeEntity1 = null;
        $serialized = $this->utils->serialize($complex);
        $deserialized = $this->utils->deserialize($serialized, ComplexEntity::class);
        $this->assertEquals($complex, $deserialized);
        $this->assertNull($deserialized->maybeEntity1);
    }

    public function testSerializeComplexEntityMissingProp() {

        $complex = new ComplexEntity();
        $complex->counter = 123;
        $complex->numOrText = "abc";
        $complex->entity1 = new SubEntity1("sub1");
        $complex->numOrNull = null;
        $this->expectException(InternalServerException::class);
        $serialized = $this->utils->serialize($complex);
    }

    public function testDeserializeComplexEntityMissingProp() {

        // This serialized version is missing the maybeEntity1 property, so it fails to deserialize.
        $serialized = '{"counter":123,"numOrText":"abc","numOrNull":null,"numOrTextOrNull":null,"entity1":{"subText1":"sub1"},"numbersArr":[],"assocNums":[],"entity1OrNull":null}';
        $this->expectException(BadRequestException::class);
        $deserialized = $this->utils->deserialize($serialized, ComplexEntity::class);
    }

    private function getStringParamTestdata(): array {

        return [
            "normal" => "normal",
            "empty" => "",
            "intString" => "1234",
            "trueString" => "true",
            "falseString" => "false",
            "nullString" => "null",
            "zeroString" => "0",
            "floatString" => "55.4",
        ];
    }

    public function testGetStringParam() {

        $params = $this->getStringParamTestdata();
        foreach ($params as $paramName => $testData) {
            $this->assertSame($testData, $this->utils->getStringParam($params, $paramName, null));
            $this->assertTrue(is_string($this->utils->getStringParam($params, $paramName, null)));
        }
        $defaultValue = "defaultValue";
        $this->assertSame($defaultValue, $this->utils->getStringParam($params, "NotFound", $defaultValue));
    }

    public function testGetOptionalStringParam() {

        $params = $this->getStringParamTestdata();
        foreach ($params as $paramName => $testData) {
            $this->assertSame($testData, $this->utils->getOptionalStringParam($params, $paramName, null));
            $this->assertTrue(is_string($this->utils->getOptionalStringParam($params, $paramName, null)));
        }
        $this->assertNull($this->utils->getOptionalStringParam($params, "NotFound", null));
    }

    private function getIntParamTestdata(): array {

        return [
            "ab" => ["1234" => 1234],
            "zero" => ["0" => 0],
            "negative" => ["-123" => -123],
            "large" => ["666777888999" => 666777888999],
        ];
    }

    private function getIntParamInvalidTestdata(): array {

        return [
            "string" => "Joda",
            "appended" => "12313Sudo",
            "prepended" => "sudo12313",
            "inside" => "123sudo12",
            "spaced" => "12 3.456",
            "false" => "false",
            "true" => "true",
            "empty" => "",
        ];
    }

    public function testGetOptionalIntParam() {

        $testParams = $this->getIntParamTestdata();
        foreach ($testParams as $paramName => $testData) {
            $inp = "" . array_key_first($testData);
            $expect = $testData[$inp];
            $params = [$paramName => $inp];
            $result = $this->utils->getOptionalIntParam($params, $paramName, null);
            $this->assertSame($expect, $result);
        }
        $invalidTestParams = $this->getIntParamInvalidTestdata();
        foreach ($invalidTestParams as $paramName => $inp) {
            try {
                $v = $this->utils->getOptionalIntParam($invalidTestParams, $paramName, null);
                $invalidTestParamsStr = implode(", ", $invalidTestParams);
                $this->fail("Did not get expected exception from getIntParam. Input $invalidTestParamsStr - $paramName got: $v");
            } catch (BadRequestException $bre) {
    // Expected exception, nothing to do
            }
        }
        $this->assertNull($this->utils->getOptionalIntParam($invalidTestParams, "NotFound", null));
    }

    public function testGetIntParam() {

        $testParams = $this->getIntParamTestdata();
        foreach ($testParams as $paramName => $testData) {
            $inp = "" . array_key_first($testData);
            $expect = $testData[$inp];
            $params = [$paramName => $inp];
            $result = $this->utils->getIntParam($params, $paramName, null);
            $this->assertSame($expect, $result);
        }
        $invalidTestParams = $this->getIntParamInvalidTestdata();
        foreach ($invalidTestParams as $paramName => $inp) {
            try {
                $v = $this->utils->getIntParam($invalidTestParams, $paramName, null);
                $invalidTestParamsStr = implode(", ", $invalidTestParams);
                $this->fail("Did not get expected exception from getIntParam. Input $invalidTestParamsStr - $paramName got: $v");
            } catch (BadRequestException $bre) {
    // Expected exception, nothing to do
            }
        }
    }

    private function getFloatParamData(): array {

        return [
            "ab" => ["1234.78" => 1234.78],
            "zero" => ["0" => 0],
            "zero2" => ["0.0" => 0.0],
            "negative" => ["-123.88" => -123.88],
            "large" => ["666777888.123" => 666777888.123],
            "int" => ["12345" => 12345],
        ];
    }

    private function getFloatParamInvalidData(): array {

        return [
            "string" => "Joda",
            "appended" => "12313Sudo",
            "prepended" => "sudo12313.12",
            "inside" => "123.02sudo12",
            "spaced" => "12 3.456",
            "false" => "false",
            "true" => "true",
            "empty" => "",
        ];
    }

    public function testGetOptionalFloatParam() {

        $testParams = $this->getFloatParamData();
        foreach ($testParams as $paramName => $testData) {
            $inp = "" . array_key_first($testData);
            $expect = $testData[$inp];
            $params = [$paramName => $inp];
            $result = $this->utils->getOptionalFloatParam($params, $paramName, null);
            $this->assertEquals($expect, $result);
        }
        $invalidTestParams = $this->getFloatParamInvalidData();
        foreach ($invalidTestParams as $paramName => $inp) {
            try {
                $v = $this->utils->getOptionalFloatParam($invalidTestParams, $paramName, null);
                $invalidTestParamsStr = implode(", ", $invalidTestParams);
                $this->fail("Did not get expected exception from getFloatParam. Input $invalidTestParamsStr - $paramName got: $v");
            } catch (BadRequestException $bre) {
    // Expected exception, nothing to do
            }
        }
        $this->assertNull($this->utils->getOptionalFloatParam($params, "NotFound", null));
    }

    public function testGetFloatParam() {

        $testParams = $this->getFloatParamData();
        foreach ($testParams as $paramName => $testData) {
            $inp = "" . array_key_first($testData);
            $expect = $testData[$inp];
            $params = [$paramName => $inp];
            $result = $this->utils->getFloatParam($params, $paramName, null);
            $this->assertEquals($expect, $result);
        }
        $invalidTestParams = $this->getFloatParamInvalidData();
        foreach ($invalidTestParams as $paramName => $inp) {
            try {
                $v = $this->utils->getFloatParam($invalidTestParams, $paramName, null);
                $invalidTestParamsStr = implode(", ", $invalidTestParams);
                $this->fail("Did not get expected exception from getFloatParam. Input $invalidTestParamsStr - $paramName got: $v");
            } catch (BadRequestException $bre) {
    // Expected exception, nothing to do
            }
        }
    }

    private function getBoolParamData(): array {

        return [
            "true" => ["true" => true],
            "false" => ["false" => false],
        ];
    }

    private function getBoolParamInvalidData(): array {

        return [
            "string" => "Joda",
            "int1" => "1",
            "int0" => "0",
            "float1" => "1.0",
            "float0" => "0.0",
            "negative1" => "-1",
            "empty" => "",
        ];
    }

    public function testGetOptionalBoolParam() {

        $testParams = $this->getBoolParamData();
        foreach ($testParams as $paramName => $testData) {
            $inp = "" . array_key_first($testData);
            $expect = $testData[$inp];
            $params = [$paramName => $inp];
            $result = $this->utils->getOptionalBoolParam($params, $paramName, null);
            $this->assertEquals($expect, $result);
        }
        $invalidTestParams = $this->getBoolParamInvalidData();
        foreach ($invalidTestParams as $paramName => $inp) {
            try {
                $v = $this->utils->getOptionalBoolParam($invalidTestParams, $paramName, null);
                $invalidTestParamsStr = implode(", ", $invalidTestParams);
                $this->fail("Did not get expected exception from getBoolParam. Input $invalidTestParamsStr - $paramName got: $v");
            } catch (BadRequestException $bre) {
    // Expected exception, nothing to do
            }
        }
    }

    public function testGetBoolParam() {

        $testParams = $this->getBoolParamData();
        foreach ($testParams as $paramName => $testData) {
            $inp = "" . array_key_first($testData);
            $expect = $testData[$inp];
            $params = [$paramName => $inp];
            $result = $this->utils->getBoolParam($params, $paramName, null);
            $this->assertEquals($expect, $result);
        }
        $invalidTestParams = $this->getBoolParamInvalidData();
        foreach ($invalidTestParams as $paramName => $inp) {
            try {
                $v = $this->utils->getBoolParam($invalidTestParams, $paramName, null);
                $invalidTestParamsStr = implode(", ", $invalidTestParams);
                $this->fail("Did not get expected exception from getBoolParam. Input $invalidTestParamsStr - $paramName got: $v");
            } catch (BadRequestException $bre) {
    // Expected exception, nothing to do
            }
        }
    }
}
