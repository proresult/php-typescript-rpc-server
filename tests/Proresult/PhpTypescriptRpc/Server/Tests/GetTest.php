<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests;

    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\ServiceUnavailableException;
    use Proresult\PhpTypescriptRpc\Server\Http;
    use Proresult\PhpTypescriptRpc\Server\Middleware\MiddlewareInterface;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\RpcRouter;
    use Proresult\PhpTypescriptRpc\Server\Tests\Models\GetSomeResponse;
    use Proresult\PhpTypescriptRpc\Server\Tests\Rpc\GetRpc;
    use Proresult\PhpTypescriptRpc\Server\Tests\RpcAdapter\GetRpcAdapter;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;

    class GetTest extends TestCase {
        private TestRequestCreator $requestCreator;
        private RequestResponseUtils $utils;

        public function __construct() {
            $this->requestCreator = new TestRequestCreator("http://localhost:8040");
            $this->utils = new RequestResponseUtils(new TestResponseFactory());
            parent::__construct();
        }

        protected function setUp() : void {
            parent::setUp();
        }

        function testGetSomeEndpoint() {
            $txt1 = "txtus1";
            $txt2 = "txtus2";
            $num1 = 3;

            $getRpc = new GetRpc();
            $response = $getRpc->getSome(txt1: $txt1, num1: $num1, txt2: $txt2);
            $expected = new GetSomeResponse($txt1, $txt2, $num1);
            $this->assertEquals($expected, $response);
        }

        /**
         * Helper method to reduce code duplication in test
         */
        private function getResponse(ServerRequestInterface $request, MiddlewareInterface ...$middleware): ResponseInterface {
            $getRpcAdapter = new GetRpcAdapter($this->utils);
            $handler = new RpcRouter(new TestResponseFactory(), "/rpc", $getRpcAdapter);
            foreach ($middleware as $mw) {
                $handler->addMiddleware($mw);
            }
            return $handler->process($request);
        }

        function testRequestResponseGetSome() {
            $params = ["txt1" => "abc", "txt2" => "def", "num1" => 123];
            $request = $this->requestCreator->rpcRequest("/rpc/GetRpc/getSome", "GET", $params);
            $response = $this->getResponse($request);
            $this->assertEquals(200, $response->getStatusCode());
            $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
            $getSomeResponse = $this->utils->deserialize($responseBody, GetSomeResponse::class);
            /** @var GetSomeResponse $getSomeResponse */
            $this->assertEquals(123,  $getSomeResponse->num1);
            $this->assertEquals("def",  $getSomeResponse->txt2);
            $this->assertEquals("abc",  $getSomeResponse->txt1);
        }

        function testRequestResponseFailingGet() {
            $params = ["fl1" => 124.23];
            $request = $this->requestCreator->rpcRequest("/rpc/GetRpc/failingGet", "GET", $params);
            $response = $this->getResponse($request);
            $this->assertEquals(200, $response->getStatusCode());

            $userfriendlyException = TestHelper::unwrapRpcResponseContainerError($response->getBody()->getContents());

            $this->assertEquals(GetRpc::FailMessage, $userfriendlyException->getMessage());
            $this->assertEquals(GetRpc::FailCode, $userfriendlyException->getCode());
        }

        function testRequestResponseWithNullableParam() {
            // First a test where one param is missing
            $params = ["txtOrNull" => "txt"]; // intOrNull not defined => should be null
            $request = $this->requestCreator->rpcRequest("/rpc/GetRpc/withNullableParam", "GET", $params);
            $response = $this->getResponse($request);
            $this->assertEquals(200, $response->getStatusCode());
            $getSomeResponse = $this->utils->deserialize(TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents()), GetSomeResponse::class);
            /** @var GetSomeResponse $getSomeResponse */
            $this->assertEquals("txt",  $getSomeResponse->txt1);
            $this->assertEquals(-100, $getSomeResponse->num1);
            // Then a test where both params are missing
            $params = [];
            $request = $this->requestCreator->rpcRequest("/rpc/GetRpc/withNullableParam", "GET", $params);
            $response = $this->getResponse($request);
            $this->assertEquals(200, $response->getStatusCode());
            $getSomeResponse = $this->utils->deserialize(TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents()), GetSomeResponse::class);
            /** @var GetSomeResponse $getSomeResponse */
            $this->assertEquals("WasNull",  $getSomeResponse->txt1);
            $this->assertEquals(-100, $getSomeResponse->num1);
        }

        function testMiddlewareHandling() {
            // This first test part is basically the same as testRequestResponseGetSome above, but with a dummy middleware doing nothing.
            $params = ["txt1" => "abc", "txt2" => "def", "num1" => 123];
            $request = $this->requestCreator->rpcRequest("/rpc/GetRpc/getSome", "GET", $params);
            $passthroughMiddleware = new Class implements MiddlewareInterface {
                public function intercept(ServerRequestInterface $request, RequestResponseInterface $next): ?ResponseInterface {
                    return $next->handleRequest($request);
                }
            };
            $response = $this->getResponse($request, $passthroughMiddleware);
            $this->assertEquals(200, $response->getStatusCode());
            $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
            $getSomeResponse = $this->utils->deserialize($responseBody, GetSomeResponse::class);
            /** @var GetSomeResponse $getSomeResponse */
            $this->assertEquals(123,  $getSomeResponse->num1);

            // Test that the request can be changed in middleware
            $requestChangingMiddleware = new Class implements MiddlewareInterface {
                public function intercept(ServerRequestInterface $request, RequestResponseInterface $next): ?ResponseInterface {
                    $params = ["txt1" => "abc", "txt2" => "def", "num1" => "666"];
                    return $next->handleRequest($request->withQueryParams($params));
                }
            };
            $response = $this->getResponse($request, $requestChangingMiddleware);
            $this->assertEquals(200, $response->getStatusCode());
            $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
            $getSomeResponse = $this->utils->deserialize($responseBody, GetSomeResponse::class);
            /** @var GetSomeResponse $getSomeResponse */
            $this->assertEquals(666,  $getSomeResponse->num1);

            // Test that the response can be changed in middleware
            $responseChangingMiddleware = new Class implements MiddlewareInterface {
                public function intercept(ServerRequestInterface $request, RequestResponseInterface $next): ?ResponseInterface {
                    $response = $next->handleRequest($request);
                    if ($response !== null) {
                        $response = $response->withStatus(204);
                    }
                    return $response;
                }
            };
            $response = $this->getResponse($request, $responseChangingMiddleware);
            $this->assertEquals(204, $response->getStatusCode());

            // Test that chaining of middleware works as expected
            $if204Return206 = new Class implements MiddlewareInterface {
                public function intercept(ServerRequestInterface $request, RequestResponseInterface $next): ?ResponseInterface {
                    $response = $next->handleRequest($request);
                    if ($response !== null && $response->getStatusCode() === 204) {
                        $response = $response->withStatus(206);
                    }
                    return $response;
                }
            };
            $response = $this->getResponse($request, $requestChangingMiddleware, $responseChangingMiddleware, $if204Return206);
            $this->assertEquals(206, $response->getStatusCode());
            $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
            $getSomeResponse = $this->utils->deserialize($responseBody, GetSomeResponse::class);
            /** @var GetSomeResponse $getSomeResponse */
            $this->assertEquals(666,  $getSomeResponse->num1);
            // The other order of middleware should give a different result
            $response = $this->getResponse($request, $if204Return206, $requestChangingMiddleware, $responseChangingMiddleware);
            $this->assertEquals(204, $response->getStatusCode());
            $responseBody = TestHelper::unwrapRpcResponseContainerResponse($response->getBody()->getContents());
            $getSomeResponse = $this->utils->deserialize($responseBody, GetSomeResponse::class);
            /** @var GetSomeResponse $getSomeResponse */
            $this->assertEquals(666,  $getSomeResponse->num1);

            // Test that throwing exceptions in middleware works as expected
            $throwingMiddleware = new Class implements MiddlewareInterface {
                public function intercept(ServerRequestInterface $request, RequestResponseInterface $next): ?ResponseInterface {
                    throw new ServiceUnavailableException("Test unavailable exception", retryAfterSeconds: 3);
                }
            };
            $response = $this->getResponse($request, $requestChangingMiddleware, $throwingMiddleware, $responseChangingMiddleware);
            $this->assertEquals(Http::STATUS_CODE_SERVICE_UNAVAILABLE, $response->getStatusCode());
            $this->assertEquals("3", $response->getHeaderLine("Retry-After"));
        }
    }