<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests\Models;

    class HelloRequest {
        public Person $to;
        public Person $from;
    }