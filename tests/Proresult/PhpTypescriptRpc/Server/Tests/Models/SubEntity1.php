<?php
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests\Models;


    class SubEntity1 {
        public string $subText1;

        /**
         * SubEntity1 constructor.
         *
         * @param string $subText1
         */
        public function __construct(string $subText1) {
            $this->subText1 = $subText1;
        }
    }