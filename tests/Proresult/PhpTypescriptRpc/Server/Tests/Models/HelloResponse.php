<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests\Models;

    use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;

    class HelloResponse {
        public string $message;
        public RpcDateTime $serverStartTime;
    }