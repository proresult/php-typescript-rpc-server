<?php
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests\Models;

    /**
     * Class ComplexEntity A entity demoing the more advanced type declarations that should work with php typescript rpc codegen.
     *
     * The generated typescript uses the number type for int and float php types.
     *
     * NB: Arrays must currently be declared as shown here, with type definition for both index and value. The more usual way to declare a numeric indexed array (e.g. string[]) is
     * not currently supported. Also, since php don't natively support defining the type of arrays, there must always be a docblock declaring it, like these examples show.
     *
     * @package Demo\Models
     */
    class ComplexEntity {
        public int $counter;
        public int | string $numOrText;
        public int | null $numOrNull;
        public int | string | null $numOrTextOrNull;
        public SubEntity1 $entity1;
        public ?SubEntity1 $maybeEntity1;
        /** @var array<int, int> */
        public array $numbersArr = [];
        /** @var array<string, int>  */
        public array $assocNums = [];
        public SubEntity1 | null $entity1OrNull = null;
    }