<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests\Models;

    class Person {
        public string $firstName;
        public string $lastName;
        public int $birthYear;

        /**
         * Person constructor.
         *
         * @param string $firstName
         * @param string $lastName
         */
        public function __construct(string $firstName, string $lastName, int $birthYear) {
            $this->firstName = $firstName;
            $this->lastName  = $lastName;
            $this->birthYear = $birthYear;
        }

        function name(): string {
            return $this->firstName." ".$this->lastName;
        }


    }