<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Server\Tests\Rpc;

use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
use Proresult\PhpTypescriptRpc\Server\Attributes\RequestType;
use Proresult\PhpTypescriptRpc\Server\Tests\EnrichedRequest;
use Proresult\PhpTypescriptRpc\Server\Tests\Models\GetSomeResponse;

#[RequestType(EnrichedRequest::class)]
class EnrichedRequestRpc {

    public function __construct(private int $requestId) {}

    static function initFromRequest(EnrichedRequest $request): EnrichedRequestRpc {
        return new EnrichedRequestRpc($request->getRequestId());
    }

    #[Get]
    public function getSome(string $a): GetSomeResponse {
        return new GetSomeResponse($a, "haba baba", $this->requestId);
    }
}