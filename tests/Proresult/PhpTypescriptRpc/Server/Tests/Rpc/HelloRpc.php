<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests\Rpc;


    use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
    use Proresult\PhpTypescriptRpc\Server\Tests\Models\HelloRequest;
    use Proresult\PhpTypescriptRpc\Server\Tests\Models\HelloResponse;
    use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;
    use Psr\Http\Message\ServerRequestInterface;

    class HelloRpc {
        private \DateTimeImmutable $startTime;

        public const failingHelloMessage = "I'm failing on purpose";
        public const failingHelloCode = 1010;
        public const failingAuthMessage = "Auth test failure";
        public const failingAuthCode = 2020;

        public function __construct(\DateTimeImmutable $startTime) {
            $this->startTime = $startTime;
        }

        public function hello(HelloRequest $request): HelloResponse {
            $response = new HelloResponse();
            $response->message = "Hello, {$request->to->name()}! Best wishes from {$request->from->name()}.";
            $response->serverStartTime = new RpcDateTime($this->startTime);
            return $response;
        }

        public function failingHello(HelloRequest $request): HelloResponse {
            if ($request->to->lastName === "retry") {
                throw new UserfriendlyException(self::failingHelloMessage, self::failingHelloCode, retryAfterSeconds: 1);
            }
            throw new UserfriendlyException(self::failingHelloMessage, self::failingHelloCode);
        }

        public function unauthorizedHello(HelloRequest $request): HelloResponse {
            throw new UnauthorizedException(self::failingAuthMessage, self::failingAuthCode);
        }

        static function initFromRequest(ServerRequestInterface $request): self {
            return new HelloRpc(new \DateTimeImmutable());
        }


    }