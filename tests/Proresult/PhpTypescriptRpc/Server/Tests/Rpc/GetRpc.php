<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests\Rpc;

    use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Retryable;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
    use Proresult\PhpTypescriptRpc\Server\Tests\Models\GetSomeResponse;
    use Psr\Http\Message\ServerRequestInterface;

    class GetRpc {
        public const FailMessage = "Failed Get";
        public const FailCode = 20122;

        static function initFromRequest(ServerRequestInterface $request): GetRpc {
            return new GetRpc();
        }

        #[Get]
        public function getSome(string $txt1, int $num1, string $txt2 = "defaultvalue"): GetSomeResponse {
            return new GetSomeResponse($txt1, $txt2, $num1);
        }

        #[Get]
        #[Retryable(0)]
        public function failingGet(float $fl1): GetSomeResponse {
            throw new UserfriendlyException(self::FailMessage, self::FailCode);
        }

        #[Get]
        public function withNullableParam(?string $txtOrNull, ?int $intOrNull): GetSomeResponse {
            return new GetSomeResponse($txtOrNull ?? "WasNull", "b", $intOrNull ?? -100);
        }
    }