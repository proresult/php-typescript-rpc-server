<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Server\Tests\Rpc;

use Proresult\PhpTypescriptRpc\Server\Attributes\AuthCheck;
use Proresult\PhpTypescriptRpc\Server\Attributes\AuthType;
use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
use Proresult\PhpTypescriptRpc\Server\Attributes\RequestType;
use Proresult\PhpTypescriptRpc\Server\Tests\AuthController1;
use Proresult\PhpTypescriptRpc\Server\Tests\EnrichedRequest;
use Proresult\PhpTypescriptRpc\Server\Tests\Models\GetSomeResponse;
use Proresult\PhpTypescriptRpc\Server\Tests\Models\HelloRequest;

#[RequestType(EnrichedRequest::class)]
#[AuthType(AuthController1::class, AuthController1::DEFAULT_ALLOW_METHOD)]
class AuthorizedRpc {

    public function __construct(private int $requestId) {}

    static function initFromRequest(EnrichedRequest $request): AuthorizedRpc {
        return new AuthorizedRpc($request->getRequestId());
    }

    // Authcheck will be a call to AuthController1->allow(). (Set as default method in Authorization attribute)
    #[Get]
    public function getSome(string $a): GetSomeResponse {
        return new GetSomeResponse($a, "haba baba", $this->requestId);
    }

    // Authcheck will be a call to AuthController1->specialAuthenticatedSome($what). (Because no method name is given in AuthCheck)
    #[Get]
    #[AuthCheck]
    public function specialAuthorizedSome(int $what): GetSomeResponse {
        return new GetSomeResponse("aaa", "bbb", $what);
    }

    // Authcheck will be a call to AuthController1->admin()
    #[Get]
    #[AuthCheck(AuthController1::REQUIRE_ADMIN)]
    public function adminRoleRequiredSome(): GetSomeResponse {
        return new GetSomeResponse("admin", "only", 333);
    }

    // Authcheck will be a call to AuthController1->projectLead($projectNum)
    #[Get]
    #[AuthCheck("projectLead", true)]
    public function projectLeadRequired(int $projectNum): GetSomeResponse {
        return new GetSomeResponse("project lead", "only", $projectNum);
    }

    // Authcheck will be a call to AuthController1->classified($hello->to->birthYear, $hello->from->birthYear)
    #[AuthCheck("classified", true, 'hello->to->birthYear', 'hello->from->birthYear')]
    public function classifiedHello(HelloRequest $hello): GetSomeResponse {
        return new GetSomeResponse("hello", $hello->to->firstName, $hello->to->birthYear);
    }
}