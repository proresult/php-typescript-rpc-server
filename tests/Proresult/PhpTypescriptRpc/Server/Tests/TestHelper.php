<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server\Tests;

    use Exception;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\BadRequestException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\InternalServerException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\MethodNotAllowedException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\NotFoundException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\NotImplementedException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\SomeRpcException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
    use Proresult\PhpTypescriptRpc\Server\Http;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\Models\RpcResponseContainer;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;

    /**
     * Used to unwrap raw ResponseBodyContainer body strings and re-encode them to json strings for deserialization with AdapterUtils.
     * Used when testing the request/response cycle of RPC
     */
    class TestHelper {
        static function unwrapRpcResponseContainerResponse(string $bodyStr): string {
            $decoded = json_decode($bodyStr, true);
            if(!array_key_exists("error", $decoded) || !array_key_exists("response", $decoded)) {
                throw new Exception("error/response prop missing from RpcResponseContainerResponse json body string");
            }
            if($decoded["error"] != null) {
                throw $decoded["error"];
            }
            return json_encode($decoded["response"]);
        }

        static function unwrapRpcResponseContainerError(string $bodyStr): UserfriendlyException {
            $decoded = json_decode($bodyStr, true);
            if(!array_key_exists("error", $decoded) || !array_key_exists("response", $decoded)) {
                throw new Exception("error/response prop missing from RpcResponseContainerResponse json body string");
            }
            if($decoded["error"] != null) {
                $error = $decoded["error"];
                $message = $error["message"];
                $code = $error["code"];
                return new UserfriendlyException($message, $code);
            } else {
                throw new Exception("error property is null");
            }
        }

        static function someRpcExceptionFromResponse(ServerRequestInterface $request, ResponseInterface $response): SomeRpcException {
            $decoded = json_decode($response->getBody()->getContents(), true, flags: JSON_THROW_ON_ERROR);
            if(array_key_exists("code", $decoded) && array_key_exists("message", $decoded)) {
                $code = intval($decoded["code"]);
                $message = $decoded["message"];
                return match ($response->getStatusCode()) {
                    Http::STATUS_CODE_BAD_REQUEST => new BadRequestException($message, $code),
                    Http::STATUS_CODE_UNAUTHORIZED => new UnauthorizedException($message, $code),
                    Http::STATUS_CODE_FORBIDDEN => new ForbiddenException($message, $code),
                    Http::STATUS_CODE_NOT_FOUND => new NotFoundException($request->getUri()->getPath(), $message, $code),
                    Http::STATUS_CODE_METHOD_NOT_ALLOWED => new MethodNotAllowedException($request, "UNKNOWN"),
                    Http::STATUS_CODE_INTERNAL_SERVER_ERROR => new InternalServerException($message, $code),
                    Http::STATUS_CODE_NOT_IMPLEMENTED => new NotImplementedException($message, $code),
                    default => throw new Exception("Could not extract SomeRpcException from response, unexpected statuscode: {$response->getStatusCode()}"),
                };
            } else {
                throw new Exception("Could not extract SomeRpcException from response. code and message properties not found in response body");
            }
        }
    }