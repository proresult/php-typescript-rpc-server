<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Server\Tests;

use DateTimeImmutable;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;

class RpcDateTimeTest extends TestCase {

    public function testCreate() {
        $dt = new DateTimeImmutable();
        $this->assertEquals($dt->getTimestamp(), RpcDateTime::create($dt)->getTimestamp());
        $this->assertNull(RpcDateTime::create(null));
    }

    public function testFromDateTimeImmutable() {
        $dt = new DateTimeImmutable();
        $this->assertEquals($dt->getTimestamp(), RpcDateTime::fromDateTimeImmutable($dt)->getTimestamp());
        $this->assertNull(RpcDateTime::fromDateTimeImmutable(null));
    }

    public function testFromString() {
        $dateTimeString = "2023-06-29T10:53:17.483+00:00";
        $bogusString = "This is bogus.";
        $this->assertEquals($dateTimeString, RpcDateTime::fromString($dateTimeString)->format(DateTimeInterface::RFC3339_EXTENDED));
        $this->expectException("Exception");
        RpcDateTime::fromString($bogusString);
    }
}