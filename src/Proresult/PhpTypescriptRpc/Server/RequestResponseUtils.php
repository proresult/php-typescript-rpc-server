<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server;

    use DateTimeInterface;
    use DateTimeZone;
    use Exception;
    use JetBrains\PhpStorm\Pure;
    use JsonException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\BadRequestException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\PropCheckException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\InternalServerException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\MethodNotAllowedException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\NotImplementedException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\ServiceUnavailableException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\SomeRpcException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
    use Proresult\PhpTypescriptRpc\Server\Models\RpcResponseContainer;
    use Proresult\PhpTypescriptRpc\Server\Normalizers\RpcDateTimeNormalizer;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;
    use Psr\Http\Message\StreamInterface;
    use ReflectionClass;
    use ReflectionException;
    use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
    use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
    use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
    use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
    use Symfony\Component\Serializer\Serializer;
    use Throwable;
    use TypeError;

/**
 * Used by the rpc adapter classes the code generator creates. To serialize rpc responses to json wire format and deserialize incoming requests into a rpc model
 * class.
 */
class RequestResponseUtils {
    public const MAX_BODY_READ_KILOBYTES = 500;
    public const MAX_BODY_READ_BYTES = self::MAX_BODY_READ_KILOBYTES * 1024;

    private Serializer $serializer;
    private ResponseFactoryInterface $responseFactory;

    /**
     * AdapterUtils constructor.
     *
     * @param DateTimeZone|null $denormalizeToTimeZone If set, RpcDateTime instances parsed from json will have this timezone set.
     */
    public function __construct(ResponseFactoryInterface $responseFactory, DateTimeZone | null $denormalizeToTimeZone = null) {
        $this->serializer = self::newSerializer($denormalizeToTimeZone);
        $this->responseFactory = $responseFactory;
    }

    private static function hasMoreContent(StreamInterface $body): bool {
        $nextByte = $body->read(1);
        return $nextByte !== ""; // Empty string signals no more content in stream
    }

    /**
     * Retrieves the request body as a string, up to the set max body length limit.
     *
     * @param ServerRequestInterface $request The incoming rpc request.
     *
     * @return string The complete request body.
     * @throws BadRequestException If the incoming request has more than allowed bytes, or some other exception happens during request body extraction. Inspect
     * thrown exception cause for details.
     */
    public function getBodyStr(ServerRequestInterface $request): string {
        try {
            $body = $request->getBody();
            try {
                $bodyStr = "";
                do {
                    // Some streams seem to read max 8kB chunks at a time, so we will repeat the read until we're done or reached the limit
                    $lastChunk = $body->read(self::MAX_BODY_READ_BYTES);
                    $bodyStr .= $lastChunk;
                    if (strlen($bodyStr) > self::MAX_BODY_READ_BYTES) {
                        $max = self::MAX_BODY_READ_KILOBYTES;
                        throw new Exception(
                            "Request body had more than $max kilobytes of data"
                        );
                    }
                } while (strlen($lastChunk) > 0);

                return $bodyStr;
            } finally {
                $body->close();
            }
        } catch (Throwable $t) {
            // Wrap any exceptions here to signal that the request was bad
            throw new BadRequestException(message: "Getting body string from request failed", previous: $t);
        }
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return array<string, string>
     */
    public function getQueryParams(ServerRequestInterface $request): array {
        $ret = $request->getQueryParams();
        /** @var array<string, string> $ret Pretty sure query params are only ever strings */
        return $ret;
    }

    /**
     * Wraps given response data in a RpcResponseContainer instance.
     * This is done before serialization to json, so that the client can distinguish between expected response and UserfriendlyException data returned from
     * server.
     *
     * @param string|int|float|bool|array|object|null $response
     *
     * @return RpcResponseContainer
     */
    #[Pure] public function responseContainer(string|int|float|bool|null|array|object $response): RpcResponseContainer {
        return new RpcResponseContainer($response);
    }

    /**
     * @throws InternalServerException
     */
    public function serialize(object $data): string {
        try {
            $reflect = new ReflectionClass($data);
            $this->propCheck($reflect->getName(), $data);
            return $this->serializer->serialize($data, "json");
        } catch (Throwable $t) {
            throw new InternalServerException(message: "Serializing response object to json failed", previous: $t);
        }
    }


    /**
     * @psalm-param class-string $type
     * @throws ReflectionException
     * @throws PropCheckException
     */
    private function propCheck(string $type, object $instance): void {
        $reflect = new ReflectionClass($type);
        $instanceVars = get_object_vars($instance);
        foreach ($reflect->getProperties() as $reflectionProperty) {
            $propName = $reflectionProperty->getName();
            if (!array_key_exists($propName, $instanceVars)) {
                throw new PropCheckException("Instance of type \"$type\" is missing declared property \"$propName\"");
            }
        }
    }

    /**
     * @template T
     * @psalm-param class-string<T> $type
     * @param string $data The data to deserialize.
     * @param string $type What type to parse into.
     *
     * @return T The parsed data json.
     * @throws BadRequestException If something goes wrong during request body deserialization.
     */
    public function deserialize(string $data, string $type): mixed {
        try {
            $ret = $this->serializer->deserialize($data, $type, "json");
            assert($ret instanceof $type);
            $this->propCheck($type, $ret);
            return $ret;
        } catch (Throwable $t) {
            $beginning = substr($data, 0, 25);
            if (!empty($beginning)) {
                $beginning .= "...";
            }
            // Wrap any exceptions here to signal that the request was bad
            throw new BadRequestException(message: "Deserializing \"$beginning\" into $type failed.", previous: $t);
        }
    }

    /**
     * @param string $responseBodyStr body (json string) to include in response.
     *
     * @throws InternalServerException If something goes wrong during response creation
     */
    public function newJsonResponse(string $responseBodyStr, int $statusCode = Http::STATUS_CODE_OK): ResponseInterface {
        try {
            return $this->responseFactory->newResponse($statusCode, $responseBodyStr, "application/json");
        } catch (Throwable $t) {
            $beginning = substr($responseBodyStr, 0, 25);
            throw new InternalServerException(message: "Creating response from \"$beginning\" failed", previous: $t);
        }
    }

    /**
     * @throws InternalServerException
     */
    public function successfulRpcJsonResponse(string|int|float|bool|null|array|object $rpcResponse): ResponseInterface {
        return $this->newJsonResponse($this->serialize($this->responseContainer($rpcResponse)));
    }

    private static function maybeWithExceptionRetry(ResponseInterface $response, SomeRpcException $exception): ResponseInterface {
        $r = $response;
        if ($exception->retryAfterSeconds !== null) {
            $r = $r->withHeader("Retry-After", "{$exception->retryAfterSeconds}");
        }
        return $r;
    }

    public function failedRpcJsonResponse(SomeRpcException $exception, int $statusCode): ResponseInterface {
        return self::maybeWithExceptionRetry($this->newJsonResponse(json_encode($exception, JSON_THROW_ON_ERROR), $statusCode), $exception);
    }

    /**
     * @throws JsonException
     * @throws InternalServerException
     */
    public function userfriendlyExceptionJsonResponse(UserfriendlyException $userfriendlyException): ResponseInterface {
        // This method does not use failedRpcJsoneResponse, because it returns a RpcResponseContainer with a success code, not a fail code.
        return self::maybeWithExceptionRetry(
            $this->newJsonResponse(json_encode(RpcResponseContainer::withError($userfriendlyException), JSON_THROW_ON_ERROR)),
            $userfriendlyException,
        );
    }

    /**
     * @throws JsonException
     * @throws InternalServerException
     */
    public function badRequestExceptionJsonResponse(BadRequestException $badRequestException): ResponseInterface {
        return $this->failedRpcJsonResponse($badRequestException, Http::STATUS_CODE_BAD_REQUEST);
    }

    /**
     * @throws JsonException
     * @throws InternalServerException
     */
    public function unauthorizedExceptionJsonResponse(UnauthorizedException $unauthorizedException): ResponseInterface {
        return $this->failedRpcJsonResponse($unauthorizedException, Http::STATUS_CODE_UNAUTHORIZED);
    }

    /**
     * @throws JsonException
     * @throws InternalServerException
     */
    public function forbiddenExceptionJsonResponse(ForbiddenException $forbiddenException): ResponseInterface {
        return $this->failedRpcJsonResponse($forbiddenException, Http::STATUS_CODE_FORBIDDEN);
    }

    /**
     * @throws InternalServerException
     * @throws JsonException
     */
    public function internalServerErrorJsonResponse(InternalServerException $internalServerException): ResponseInterface {
        return $this->failedRpcJsonResponse($internalServerException, Http::STATUS_CODE_INTERNAL_SERVER_ERROR);
    }

    public function serviceUnavailableExceptionJsonResponse(ServiceUnavailableException $serviceUnavailableException): ResponseInterface {
        return $this->failedRpcJsonResponse($serviceUnavailableException, Http::STATUS_CODE_SERVICE_UNAVAILABLE);
    }

    /**
     * @throws JsonException
     * @throws InternalServerException
     * @noinspection PhpUnusedParameterInspection
     */
    public function notFoundResponse(ServerRequestInterface $request): ResponseInterface {
        return $this->newJsonResponse(
            json_encode(["code" => 404, "message" => "No RPC for given URL found"], JSON_THROW_ON_ERROR),
            Http::STATUS_CODE_NOT_FOUND
        );
    }

    /**
     * @throws JsonException
     * @throws InternalServerException
     */
    public function notImplementedExceptionJsonResponse(NotImplementedException $notImplementedException): ResponseInterface {
        return $this->failedRpcJsonResponse($notImplementedException, Http::STATUS_CODE_NOT_IMPLEMENTED);
    }

    /**
     * @throws InternalServerException
     * @throws JsonException
     */
    public function methodNotAllowedExceptionJsonResponse(MethodNotAllowedException $methodNotAllowedException): ResponseInterface {
        $response = $this->failedRpcJsonResponse($methodNotAllowedException, Http::STATUS_CODE_METHOD_NOT_ALLOWED);
        return $response->withAddedHeader("Allow", $methodNotAllowedException->getAllowedMethod());
    }

    protected static function newSerializer(DateTimeZone | null $denormalizeToTimeZone): Serializer {
        $extractor = new PropertyInfoExtractor([], [new PhpDocExtractor(), new ReflectionExtractor()]);
        $normalizers = [
            new ArrayDenormalizer(),
            new RpcDateTimeNormalizer($denormalizeToTimeZone, [DateTimeNormalizer::FORMAT_KEY => DateTimeInterface::RFC3339_EXTENDED]),
            new ObjectNormalizer(null, null, null, $extractor)
        ];
        $encoders = [new JsonEncoder()];
        return new Serializer($normalizers, $encoders);
    }

    /**
     * @throws MethodNotAllowedException
     */
    public function ensureMethod(ServerRequestInterface $request, string $method): void {
        if ($request->getMethod() !== $method) {
            throw new MethodNotAllowedException($request, $method);
        }
    }

    /**
     * @param array<string, string> $params
     * @throws BadRequestException
     */
    public function getOptionalStringParam(array $params, string $paramName, ?string $defaultValue): ?string {
        if (array_key_exists($paramName, $params)) {
            try {
                return $params[$paramName];
            } catch (TypeError $typeError) {
                throw new BadRequestException("GET parameter value $paramName is not a string", 0, $typeError);
            }
        } else {
            return $defaultValue;
        }
    }

    /**
     * @param array<string, string> $params
     * @throws BadRequestException
     */
    public function getStringParam(array $params, string $paramName, ?string $defaultValue): string {
        $maybeString = $this->getOptionalStringParam($params, $paramName, $defaultValue);
        if ($maybeString !== null) {
            return $maybeString;
        } else {
            if ($defaultValue !== null) {
                return $defaultValue;
            } else {
                throw new BadRequestException("Required GET parameter \"$paramName\" not found in request, and no default value provided");
            }
        }
    }

    /**
     * @param array<string, string> $params
     * @throws BadRequestException
     */
    public function getOptionalIntParam(array $params, string $paramName, ?int $defaultValue): ?int {
        if (array_key_exists($paramName, $params)) {
            $v = $params[$paramName];
            $intV = intval($v);
            if (strval($intV) === $v) {
                return $intV;
            } else {
                throw new BadRequestException("GET parameter value " . print_r($v, true) . " could not safely be converted to a int");
            }
        } else {
            return $defaultValue;
        }
    }
        /**
     * @param array<string, string> $params
     * @throws BadRequestException
     */
    public function getIntParam(array $params, string $paramName, ?int $defaultValue): int {
        $maybeInt = $this->getOptionalIntParam($params, $paramName, $defaultValue);
        if ($maybeInt !== null) {
            return $maybeInt;
        } else {
            if ($defaultValue !== null) {
                return $defaultValue;
            } else {
                throw new BadRequestException("Required GET parameter \"$paramName\" not found in request, and no default value provided");
            }
        }
    }

    /**
     * @param array<string, string> $params
     * @throws BadRequestException
     */
    public function getOptionalFloatParam(array $params, string $paramName, ?float $defaultValue): ?float {
        if (array_key_exists($paramName, $params)) {
            $v = $params[$paramName];
            $floatV = floatval($v);
            if (strval($floatV) == $v) {
                return $floatV;
            } else {
                /** @noinspection PhpUnnecessaryStringCastInspection */
                throw new BadRequestException("GET parameter value \"$v\" could not safely be converted to a float. Became: \"" . strval($floatV) . "\"");
            }
        } else {
            return $defaultValue;
        }
    }

    /**
     * @param array<string, string> $params
     * @throws BadRequestException
     */
    public function getFloatParam(array $params, string $paramName, ?float $defaultValue): float {
        $maybeFloat = $this->getOptionalFloatParam($params, $paramName, $defaultValue);
        if ($maybeFloat !== null) {
            return $maybeFloat;
        } else {
            if ($defaultValue !== null) {
                return $defaultValue;
            } else {
                throw new BadRequestException("Required GET parameter \"$paramName\" not found in request, and no default value provided");
            }
        }
    }

    /**
     * @param array<string, string> $params
     * @throws BadRequestException
     */
    public function getOptionalBoolParam(array $params, string $paramName, ?bool $defaultValue): ?bool {
        if (array_key_exists($paramName, $params)) {
            $v = $params[$paramName];
            if ($v === "true") {
                return true;
            } elseif ($v === "false") {
                return false;
            } else {
                throw new BadRequestException("GET parameter value " . print_r($v, true) . " not a valid bool. Must be \"true\" or \"false\"");
            }
        } else {
            return $defaultValue;
        }
    }

    /**
     * @param array<string, string> $params
     * @throws BadRequestException
     */
    public function getBoolParam(array $params, string $paramName, ?bool $defaultValue): bool {
        $maybeBool = $this->getOptionalBoolParam($params, $paramName, $defaultValue);
        if ($maybeBool !== null) {
            return $maybeBool;
        } else {
            if ($defaultValue !== null) {
                return $defaultValue;
            } else {
                throw new BadRequestException("Required GET parameter \"$paramName\" not found in request, and no default value provided");
            }
        }
    }
}
