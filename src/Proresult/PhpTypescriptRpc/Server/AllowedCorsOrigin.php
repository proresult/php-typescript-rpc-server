<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server;

use InvalidArgumentException;
use JetBrains\PhpStorm\Pure;

/**
     * Model class used to hold settings value for the CorsProcessor.
     */
class AllowedCorsOrigin {
    public string $protocol;
    public string $host;
    public int $port;
    public bool $includingSubDomains;

    public const ALLOWED_PROTOCOL = ["http", "https"];

    // This is not a perfect validation that ensures that passed host values result in a working CORS. Just a friendly helper to avoid honest mistakes.
    public static function validateHost(string $host, bool $includingSubDomains): void {
        // Host cannot be empty or *
        $host = trim($host);
        if ($host === "") {
            throw new InvalidArgumentException("Empty host value is not allowed");
        }
        if (strlen($host) < 3) {
            throw new InvalidArgumentException("Host value must be at least three characters");
        }
        // Host cannot start or end with "."
        if (str_starts_with($host, ".") || str_ends_with($host, ".")) {
            throw new InvalidArgumentException('Host value cannot start or end with "." is not allowed');
        }
        // If includingSubDomains is true, host must have at least one "." in it, to avoid accidental TLD allowance.
        if ($includingSubDomains && !str_contains($host, ".")) {
            throw new InvalidArgumentException('Host value must contain at least one "." when including subdomains as allowed CORS origins');
        }
    }

/**
     * AllowedCorsOrigin constructor.
     *
     * @param string $protocol
     * @param string $host
     * @param int    $port
     */
    public function __construct(string $protocol, string $host, int $port, bool $includingSubDomains = false) {
        if (in_array($protocol, self::ALLOWED_PROTOCOL)) {
            $this->protocol = $protocol;
        } else {
            $allowed = join(", ", self::ALLOWED_PROTOCOL);
            throw new InvalidArgumentException("protocol argument \"${protocol}\" is not among allowed argument values ($allowed)");
        }
        self::validateHost($host, $includingSubDomains);
        $this->host     = $host;
        if ($port <= 0) {
            throw new InvalidArgumentException("port number $port is not a legal value. Must be positive");
        }
        $this->port     = $port;
        $this->includingSubDomains = $includingSubDomains;
    }

    #[Pure] public function originString(): string {
        return $this->protocol . "://" . $this->host . ":" . $this->port;
    }

    #[Pure]
    private static function hasSubDomain(string $origin): bool {
        return count(explode(".", $origin, 4)) > 2;
    }

    /**
     * Checks that incoming request origin matches allowed CORS origin
     */
    #[Pure] public function match(string $origin): bool {
        if ($this->includingSubDomains && self::hasSubDomain($origin)) {
            // If we allow any subdomain we just check that the parts before and after subdomain is as expected.
            $protocolOk = str_starts_with($origin, $this->protocol . "://");
            // Since the incoming origin has sub domain, require a leading dot before the main domain in the end match.
            $expectedEnd = ".{$this->host}";
            // Try checking both with and without port number, since it's ok if port number is default
            $expectedEndWithPort = "{$expectedEnd}:{$this->port}";
            $endOk = str_ends_with($origin, $expectedEnd) || str_ends_with($origin, $expectedEndWithPort);
            return $protocolOk && $endOk;
        } else {
            // Since port might be the default, we check both with and without it
            $expected = "{$this->protocol}://{$this->host}";
            $expectedWithPort = "{$expected}:{$this->port}";
            return $origin === $expected || $origin === $expectedWithPort;
        }
    }
}
