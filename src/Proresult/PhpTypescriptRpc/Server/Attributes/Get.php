<?php

/* @noinspection PhpMultipleClassDeclarationsInspection */

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class Get {
}
