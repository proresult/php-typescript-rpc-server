<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class AuthCheck {
    /** @var string|null If this is null, the method name the attribute is attached to will be used. */
    public ?string $methodName = null;
/** @var bool If true, include method parameters in call to auth check method. */
    public bool $includeParams;
/** @var string[] If this has any elements, and includeParams is true, they will be used as parameters to the auth method instead of using the attached method arguments directly */
    public array $paramMaps = [];
/**
     * @param string|null $methodName
     * @param bool|null   $includeParams Default value is true if $methodName is NOT set, false if it is set
     * @param string      ...$paramMaps
     */
    public function __construct(?string $methodName = null, ?bool $includeParams = null, string ...$paramMaps,) {

        if ($methodName !== null) {
            $this->methodName = trim($methodName);
            if (strlen($this->methodName) === 0) {
                $this->methodName = null;
            }
        }
        if ($includeParams === null) {
            $this->includeParams = $this->methodName === null;
        } else {
            $this->includeParams = $includeParams;
        }
        $this->paramMaps = $paramMaps;
    }
}
