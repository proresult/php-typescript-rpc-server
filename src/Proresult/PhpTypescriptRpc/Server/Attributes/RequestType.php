<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class RequestType {
    public function __construct(public string $requestClass) {
    }
}
