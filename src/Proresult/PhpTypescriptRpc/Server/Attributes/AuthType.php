<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class AuthType {
    public function __construct(public string $controllerClass, public ?string $defaultMethodName = null) {
    }
}
