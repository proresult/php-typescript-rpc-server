<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server;

class Http {
    public const STATUS_CODE_OK = 200;
    public const STATUS_CODE_BAD_REQUEST = 400;
    public const STATUS_CODE_UNAUTHORIZED = 401;
    public const STATUS_CODE_FORBIDDEN = 403;
    public const STATUS_CODE_NOT_FOUND = 404;
    public const STATUS_CODE_METHOD_NOT_ALLOWED = 405;
    public const STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
    public const STATUS_CODE_NOT_IMPLEMENTED = 501;
    public const STATUS_CODE_SERVICE_UNAVAILABLE = 503;
}
