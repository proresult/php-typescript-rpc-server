<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Middleware;

use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * One or more MiddlewareInterface instances can be defined and passed to the RpcRouter (addMiddleware) to add a chain of request/response handlers that may
 * provide extra functionality for the rpc server.
 *
 * May e.g. be used to add circuit breaker, rate limiting, etc.
 */
interface MiddlewareInterface {
    public function intercept(ServerRequestInterface $request, RequestResponseInterface $next): ?ResponseInterface;
}
