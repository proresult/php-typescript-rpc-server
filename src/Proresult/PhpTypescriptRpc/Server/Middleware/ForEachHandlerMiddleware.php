<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Middleware;

use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * This is used in RpcRouter. All rpc classes the RpcRouter is to work with is put in one of these to make it the innermost Middleware in the
 * MiddlewareContainer chain. This makes for a clean Middleware functionality.
 */
class ForEachHandlerMiddleware implements MiddlewareInterface {
    public function __construct(/** @var RequestResponseInterface[] */
        private array $handlers,
    ) {
    }

    public function intercept(ServerRequestInterface $request, RequestResponseInterface $next): ?ResponseInterface {

        // Process GET/POST request:
        foreach ($this->handlers as $handler) {
            $maybeRpcResponse = $handler->handleRequest($request);
            if ($maybeRpcResponse != null) {
                return $maybeRpcResponse;
            }
        }
        return $next->handleRequest($request);
    }
}
