<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Middleware;

use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class NoNextRequestResponse implements RequestResponseInterface {
    public function handleRequest(ServerRequestInterface $request): ?ResponseInterface {

        return null;
    }
}
