<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Middleware;

use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * MiddlewareContainer is used to build a "recursive" chain of middleware request/response handlers. For use internally in RpcRouter.
 */
class MiddlewareContainer implements RequestResponseInterface {
    public function __construct(private MiddlewareInterface $middleware, private ?MiddlewareContainer $next) {
    }

    public function handleRequest(ServerRequestInterface $request): ?ResponseInterface {

        return $this->middleware->intercept($request, $this->next ?? new NoNextRequestResponse());
    }
}
