<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Normalizers;

use DateTimeZone;
use Exception;
use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

/**
 * Class RpcDateTimeNormalizer Minimal extension to denormalize into RpcDateTime instead of DateTime/DateTimeImmutable
 *
 * @package Proresult\PhpTypescriptRpc\Server\Normalizers
 */
class RpcDateTimeNormalizer extends DateTimeNormalizer {
    /**
     * @param DateTimeZone|null $denormalizeToTimeZone If set, the denormalizer will change timezone of denormalized RpcDateTime into this.
     * @param array             $defaultContext
     */
    public function __construct(
        private DateTimeZone | null $denormalizeToTimeZone,
        array $defaultContext = []
    ) {
        parent::__construct($defaultContext);
    }

    /**
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): RpcDateTime {
        $dateTime = new RpcDateTime(parent::denormalize($data, $type, $format, $context));
        if ($this->denormalizeToTimeZone === null) {
            return $dateTime;
        }

        return $dateTime->setTimezone($this->denormalizeToTimeZone);
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool {
        return $type == RpcDateTime::class;
    }
}
