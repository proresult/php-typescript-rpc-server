<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server;

use Proresult\PhpTypescriptRpc\Server\Exceptions\BadRequestException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\InternalServerException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\MethodNotAllowedException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\NotFoundException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\NotImplementedException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\ServiceUnavailableException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
use Throwable;

/**
     * RPC server implementors should inject an implementation of this interface into the RpcRouter instance, to get exceptions in request processing logged.
     */
interface MonitoringInterface {
    public function reportUserfriendlyException(UserfriendlyException $e): void;
    public function reportBadRequestException(BadRequestException $e): void;
    public function reportUnauthorizedException(UnauthorizedException $e): void;
    public function reportForbiddenException(ForbiddenException $e): void;
    public function reportInternalServerException(InternalServerException $e): void;
    public function reportOtherException(Throwable $e): void;
    public function reportMethodNotAllowedException(MethodNotAllowedException $e): void;
    public function reportNotImplementedException(NotImplementedException $e): void;
    public function reportNotFoundException(NotFoundException $e): void;
    public function reportServiceUnavailableException(ServiceUnavailableException $e): void;
}
