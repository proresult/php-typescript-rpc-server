<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

abstract class WrappedRequest implements ServerRequestInterface {
    public function __construct(protected ServerRequestInterface $wrapped) {
    }

    /**
     * @inheritDoc
     */
    public function getProtocolVersion() {

        return $this->wrapped->getProtocolVersion();
    }

    /**
     * @inheritDoc
     */
    public function withProtocolVersion($version) {

        $this->wrapped = $this->wrapped->withProtocolVersion($version);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getHeaders() {

        return $this->wrapped->getHeaders();
    }

    /**
     * @inheritDoc
     */
    public function hasHeader($name) {

        return $this->wrapped->hasHeader($name);
    }

    /**
     * @inheritDoc
     */
    public function getHeader($name) {

        return $this->wrapped->getHeader($name);
    }

    /**
     * @inheritDoc
     */
    public function getHeaderLine($name) {

        return $this->wrapped->getHeaderLine($name);
    }

    /**
     * @inheritDoc
     */
    public function withHeader($name, $value) {

        $this->wrapped = $this->wrapped->withHeader($name, $value);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function withAddedHeader($name, $value) {

        $this->wrapped = $this->wrapped->withAddedHeader($name, $value);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function withoutHeader($name) {

        $this->wrapped = $this->wrapped->withoutHeader($name);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBody() {

        return $this->wrapped->getBody();
    }

    /**
     * @inheritDoc
     */
    public function withBody(StreamInterface $body) {

        $this->wrapped = $this->wrapped->withBody($body);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRequestTarget() {

        return $this->wrapped->getRequestTarget();
    }

    /**
     * @inheritDoc
     */
    public function withRequestTarget($requestTarget): self {

        $this->wrapped = $this->wrapped->withRequestTarget($requestTarget);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getMethod() {

        return $this->wrapped->getMethod();
    }

    /**
     * @inheritDoc
     */
    public function withMethod($method) {

        $this->wrapped = $this->wrapped->withMethod($method);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUri() {

        return $this->wrapped->getUri();
    }

    /**
     * @inheritDoc
     */
    public function withUri(UriInterface $uri, $preserveHost = false) {

        $this->wrapped = $this->wrapped->withUri($uri, $preserveHost);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getServerParams() {

        return $this->wrapped->getServerParams();
    }

    /**
     * @inheritDoc
     */
    public function getCookieParams() {

        return $this->wrapped->getCookieParams();
    }

    /**
     * @inheritDoc
     */
    public function withCookieParams(array $cookies) {

        $this->wrapped = $this->wrapped->withCookieParams($cookies);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getQueryParams() {

        return $this->wrapped->getQueryParams();
    }

    /**
     * @inheritDoc
     */
    public function withQueryParams(array $query) {

        $this->wrapped = $this->wrapped->withQueryParams($query);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUploadedFiles() {

        return $this->wrapped->getUploadedFiles();
    }

    /**
     * @inheritDoc
     */
    public function withUploadedFiles(array $uploadedFiles) {

        $this->wrapped = $this->wrapped->withUploadedFiles($uploadedFiles);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getParsedBody() {

        return $this->wrapped->getParsedBody();
    }

    /**
     * @inheritDoc
     */
    public function withParsedBody($data) {

        $this->wrapped = $this->wrapped->withParsedBody($data);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getAttributes() {

        return $this->wrapped->getAttributes();
    }

    /**
     * @inheritDoc
     */
    public function getAttribute($name, $default = null) {

        return $this->wrapped->getAttribute($name, $default);
    }

    /**
     * @inheritDoc
     */
    public function withAttribute($name, $value) {

        $this->wrapped = $this->wrapped->withAttribute($name, $value);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function withoutAttribute($name) {

        $this->wrapped = $this->wrapped->withoutAttribute($name);
        return $this;
    }
}
