<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use JetBrains\PhpStorm\Pure;
use Throwable;

/**
     * Thrown by request handling code when incoming request is invalid.
     * Should cause a 400 status code response to be returned to requester.
     */
class BadRequestException extends SomeRpcException {
    #[Pure] public function __construct(string $message = "", int $code = 0, Throwable $previous = null) {
        if (empty($message)) {
            $message = "Bad RPC request";
        }
        parent::__construct($message, $code, $previous);
    }
}
