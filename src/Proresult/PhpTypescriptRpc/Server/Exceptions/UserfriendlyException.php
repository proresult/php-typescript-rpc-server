<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use JetBrains\PhpStorm\Pure;
use Throwable;

/**
     * UserfriendlyException can be used to signal a error that should be returned as a OK response (status code 200).
     * The body of the response will contain a error code and a error message in english.
     * The client will then throw a async rejection that contains the error data from server, so that the web frontend can display a friendly
     * (localized) error message to the user.
     */
class UserfriendlyException extends SomeRpcException {
    /**
     * @param string         $message Error message in english to propagate to client. NB: Must not contain sensitive internal data!
     * @param int            $code Error code. Must be zero (for undefined), or above 1000 for a specific error.
     * @param Throwable|null $previous
     */
    #[Pure] public function __construct(string $message = "", int $code = 0, Throwable $previous = null, ?int $retryAfterSeconds = null) {
        if (empty($message)) {
            $message = "Some unexpected failure happened during server processing";
        }
        parent::__construct($message, $code, $previous, $retryAfterSeconds);
    }
}
