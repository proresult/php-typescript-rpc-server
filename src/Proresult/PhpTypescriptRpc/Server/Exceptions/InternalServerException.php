<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use JetBrains\PhpStorm\Pure;
use Throwable;

class InternalServerException extends SomeRpcException {
    #[Pure] public function __construct(string $message = "", int $code = 0, Throwable $previous = null, ?int $retryAfterSeconds = null) {
        if (empty($message)) {
            $message = "Internal RPC server error";
        }
        parent::__construct($message, $code, $previous, $retryAfterSeconds);
    }
}
