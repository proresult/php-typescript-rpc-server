<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use JetBrains\PhpStorm\Pure;
use Throwable;

class NotImplementedException extends SomeRpcException {
    #[Pure] public function __construct(string $message = "", int $code = 0, Throwable $previous = null) {
        if (empty($message)) {
            $message = "Not Implemented";
        }
        parent::__construct($message, $code, $previous);
    }
}
