<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use JetBrains\PhpStorm\Pure;
use Throwable;

class ServiceUnavailableException extends SomeRpcException {
    public const DEFAULT_MESSAGE = "Service temporary unavailable.";

    #[Pure] public function __construct(string $message = "", int $code = 0, Throwable $previous = null, ?int $retryAfterSeconds = 10) {
        if (empty($message)) {
            $message = self::DEFAULT_MESSAGE;
        }
        parent::__construct($message, $code, $previous, $retryAfterSeconds);
    }
}
