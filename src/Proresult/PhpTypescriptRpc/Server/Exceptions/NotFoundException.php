<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use JetBrains\PhpStorm\Pure;
use Throwable;

class NotFoundException extends SomeRpcException {
    private string $requestPath;
    #[Pure] public function __construct(string $requestPath, string $message = "", int $code = 0, Throwable $previous = null) {
        $this->requestPath = $requestPath;
        if (empty($message)) {
            $message = "No RPC found at request path \"${requestPath}\"";
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     * @noinspection PhpUnused
     */
    public function getRequestPath(): string {
        return $this->requestPath;
    }
}
