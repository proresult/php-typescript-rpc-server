<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use Proresult\PhpTypescriptRpc\Server\Http;
use Psr\Http\Message\ServerRequestInterface;

class MethodNotAllowedException extends SomeRpcException {
    private ServerRequestInterface $request;
    private string $allowedMethod;
    public function __construct(ServerRequestInterface $request, string $allowedMethod) {
        $this->request = $request;
        $this->allowedMethod = $allowedMethod;
        $message = "Method \"{$request->getMethod()}\" Not Allowed";
        parent::__construct($message, Http::STATUS_CODE_METHOD_NOT_ALLOWED);
    }

    public function getAllowedMethod(): string {
        return $this->allowedMethod;
    }
}
