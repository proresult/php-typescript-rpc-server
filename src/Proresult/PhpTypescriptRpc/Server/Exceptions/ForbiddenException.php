<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use JetBrains\PhpStorm\Pure;
use Throwable;

class ForbiddenException extends SomeRpcException {
    public const DEFAULT_MESSAGE = "Authenticated request is not authorized to call this rpc";
    #[Pure] public function __construct(string $message = "", int $code = 0, Throwable $previous = null) {

        if (empty($message)) {
            $message = self::DEFAULT_MESSAGE;
        }
        parent::__construct($message, $code, $previous);
    }
}
