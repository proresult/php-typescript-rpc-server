<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use Exception;
use JetBrains\PhpStorm\Pure;
use Throwable;

class PropCheckException extends Exception {
    #[Pure] public function __construct(string $message = "", int $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
