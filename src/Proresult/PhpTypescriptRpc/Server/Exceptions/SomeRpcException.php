<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;
use Throwable;

abstract class SomeRpcException extends Exception implements JsonSerializable {
    /**
     * @var int|null If set, the response will have the "Retry-After header set accordingly.
     */
    public ?int $retryAfterSeconds = null;

    #[Pure] public function __construct(string $message = "", int $code = 0, Throwable $previous = null, ?int $retryAfterSeconds = null) {
        $this->retryAfterSeconds = $retryAfterSeconds;
        parent::__construct($message, $code, $previous);
    }

    #[Pure] #[ArrayShape(["code" => "int|mixed", "message" => "string"])] public function jsonSerialize(): array {
        return ["code" => $this->getCode(), "message" => $this->getMessage()];
    }
}
