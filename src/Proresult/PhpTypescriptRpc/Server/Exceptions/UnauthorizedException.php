<?php

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Exceptions;

use JetBrains\PhpStorm\Pure;
use Throwable;

/**
 * This exception is used to signal missing or invalid authentication. Named Unauthorized to match the http status name it corresponds to, even though it
 * should have been named "UnauthenticatedException".
 */
class UnauthorizedException extends SomeRpcException {
    public const DEFAULT_MESSAGE = "RPC request did not provide valid authentication";
    #[Pure] public function __construct(string $message = "", int $code = 0, Throwable $previous = null) {

        if (empty($message)) {
            $message = self::DEFAULT_MESSAGE;
        }
        parent::__construct($message, $code, $previous);
    }
}
