<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server;

use Proresult\PhpTypescriptRpc\Server\Exceptions\BadRequestException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\InternalServerException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\MethodNotAllowedException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\NotFoundException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\NotImplementedException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\ServiceUnavailableException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
use Throwable;

class DefaultMonitoringImplementation implements MonitoringInterface {
    protected function logMessageFromThrowable(Throwable $t): string {
        $exceptionTxt = "";
        do {
            $exceptionTxt .= get_class($t) . " - " . $t->getMessage() . " (at '" . $t->getFile() . "', line " . $t->getLine() . ")" . PHP_EOL;
            $stackTraceTxt = $t->getTraceAsString();
        // Deepest exception has deepest stacktrace
            $t = $t->getPrevious();
        } while ($t != null);
        $exceptionOutput = "";
        if (!empty($exceptionTxt)) {
            $exceptionOutput .= PHP_EOL . $exceptionTxt;
        }
        if (!empty($stackTraceTxt)) {
            $exceptionOutput .= "Stacktrace: " . $stackTraceTxt;
        }
        return $exceptionOutput;
    }

    protected function logThrowable(int $priority, Throwable $t): void {
        syslog($priority, $this->logMessageFromThrowable($t));
    }

    public function reportUserfriendlyException(UserfriendlyException $e): void {
        $this->logThrowable(LOG_WARNING, $e);
    }

    public function reportBadRequestException(BadRequestException $e): void {
        $this->logThrowable(LOG_WARNING, $e);
    }

    public function reportUnauthorizedException(UnauthorizedException $e): void {
        $this->logThrowable(LOG_WARNING, $e);
    }

    public function reportForbiddenException(ForbiddenException $e): void {
        $this->logThrowable(LOG_WARNING, $e);
    }

    public function reportInternalServerException(InternalServerException $e): void {
        $this->logThrowable(LOG_ERR, $e);
    }

    public function reportOtherException(Throwable $e): void {
        $this->logThrowable(LOG_ERR, $e);
    }

    public function reportMethodNotAllowedException(MethodNotAllowedException $e): void {
        $this->logThrowable(LOG_WARNING, $e);
    }

    public function reportNotImplementedException(NotImplementedException $e): void {
        $this->logThrowable(LOG_WARNING, $e);
    }

    public function reportNotFoundException(NotFoundException $e): void {
        $this->logThrowable(LOG_WARNING, $e);
    }

    public function reportServiceUnavailableException(ServiceUnavailableException $e): void {
        $this->logThrowable(LOG_WARNING, $e);
    }
}
