<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Models;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;

/**
 * Class RpcDateTime A wrapper around DateTimeImmutable, to get exact same result when we serialize and deserialize to/from json for date values.
 *
 * The builtin DateTime and DateTimeImmutable php classes use microsecond accuracy. The standard browser date json serialization only uses millisecond accuracy.
 *
 * To ensure that a date time value returned from rpc framework will be the same if the client sends it back, we define this wrapper that truncates the wrapped
 * DateTimeImmutable to millisecond accuracy on creation. This way we get the same value back after serialization/deserialization to standard json format.
 *
 * @package Proresult\PhpTypescriptRpc\Server\Models
 */
class RpcDateTime extends DateTimeImmutable {
    /**
     * RpcDateTime constructor.
     *
     * @throws Exception
     */
    public function __construct(DateTimeInterface $dateTime) {
        /** @psalm-suppress ImpureMethodCall - The format and getTimezone methods do not mutate anything */
        parent::__construct($dateTime->format(DateTimeInterface::RFC3339_EXTENDED), $dateTime->getTimezone());
    }

    /**
     * Constructs a new RpcDateTime if given argument is not null.
     *
     * @deprecated Will be removed in a future version. Use {@see RpcDateTime::fromDateTimeImmutable()} instead.
     * @param DateTimeImmutable|null $dateTimeOrNull An instance of a class implementing DateTimeInterface.
     * @return RpcDateTime|null An instance of RpcDateTime if $dateTimeOrNull is not null, null otherwise.
     * @throws Exception
     */
    public static function create(?DateTimeInterface $dateTimeOrNull): ?RpcDateTime {
        return self::fromDateTimeImmutable($dateTimeOrNull);
    }

    /**
     * Constructs a new RpcDateTime if given argument is not null.
     *
     * @param DateTimeInterface|null $dateTimeOrNull An instance of a class implementing DateTimeInterface.
     * @return RpcDateTime|null An instance of RpcDateTime if $dateTimeOrNull is not null, null otherwise.
     * @throws Exception
     */
    public static function fromDateTimeImmutable(?DateTimeInterface $dateTimeOrNull): ?RpcDateTime {
        return $dateTimeOrNull !== null ? new RpcDateTime($dateTimeOrNull) : null;
    }

    /**
     * Constructs a new RpcDateTime from given DateTime string.
     * Throws an exception if the DateTime string is invalid.
     *
     * @param string $dateTimeString
     * @return RpcDateTime
     * @throws Exception
     */
    public static function fromString(string $dateTimeString): RpcDateTime {
        return new RpcDateTime(new DateTimeImmutable($dateTimeString));
    }
}
