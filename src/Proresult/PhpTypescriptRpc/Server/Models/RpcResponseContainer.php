<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server\Models;

use JetBrains\PhpStorm\Pure;
use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;

/**
     * Class RpcResponseContainer wraps all responses from rpc server.
     * If the rpc has the expected success-type, the response property is set.
     * If the rpc failed during processing, the error property is set to a UserfriendlyException value to indicate what failed.
     *
     * Also declared in php-typescript-rpc-client typescript library code.
     */
class RpcResponseContainer {
    public string|int|float|bool|null|array|object $response = null;
    public UserfriendlyException|null $error = null;
/**
     * @param array|bool|float|int|object|string|null $response
     */
    public function __construct(float|object|array|bool|int|string|null $response, UserfriendlyException|null $error = null) {
        $this->response = $response;
        $this->error = $error;
    }

    #[Pure] public static function withError(UserfriendlyException $error): self {
        return new RpcResponseContainer(null, $error);
    }
}
