<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server;

use Psr\Http\Message\ResponseInterface;

/**
     * Interface ResponseFactory should implement a method that returns a PSR-7 Response.
     * The response status code shall be the one given in $statusCode argument.
     * If provided, given $body argument shall be set as the response body.
     * If provided, given Content-Type shall be set in the response header.
     *
     * Used by request processors to return errors, CORS responses, etc.
     *
     * @package Proresult\PhpTypescriptRpc\Server
     */
interface ResponseFactoryInterface {
    public function newResponse(int $statusCode, ?string $body, ?string $contentType): ResponseInterface;
}
