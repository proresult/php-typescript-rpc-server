<?php

/** @noinspection PhpUnnecessaryLocalVariableInspection */

/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Server;

use DateTimeZone;
use JsonException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\BadRequestException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\InternalServerException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\MethodNotAllowedException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\NotFoundException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\NotImplementedException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\ServiceUnavailableException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;
use Proresult\PhpTypescriptRpc\Server\Exceptions\UserfriendlyException;
use Proresult\PhpTypescriptRpc\Server\Middleware\ForEachHandlerMiddleware;
use Proresult\PhpTypescriptRpc\Server\Middleware\MiddlewareContainer;
use Proresult\PhpTypescriptRpc\Server\Middleware\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

class RpcRouter {
    private string $basePath;

    private ?CorsProcessor $corsProcessor = null;

    private RequestResponseUtils $requestResponseUtils;
    private MonitoringInterface $monitoring;

    private MiddlewareContainer $middlewareChain;

    /**
     * RpcRouter constructor.
     *
     *
     * @throws InternalServerException
     */
    public function __construct(
        ResponseFactoryInterface $responseFactory,
        string $basePath = "",
        RequestResponseInterface ...$rpcClasses
    ) {
        $this->requestResponseUtils = new RequestResponseUtils($responseFactory);
        $this->basePath = $basePath;
        if (empty($rpcClasses)) {
            throw new InternalServerException("No endpoint classes provided.");
        }
        if (!empty($basePath)) {
            $this->validatePath($basePath);
        }
        $this->middlewareChain = new MiddlewareContainer(new ForEachHandlerMiddleware($rpcClasses), null);
        $this->monitoring = new DefaultMonitoringImplementation();
    }

    public function setCorsProcessor(CorsProcessor $corsProcessor): self {
        $this->corsProcessor = $corsProcessor;
        return $this;
    }

    public function setMonitoringInterface(MonitoringInterface $monitoring): self {
        $this->monitoring = $monitoring;
        return $this;
    }

    public function addMiddleware(MiddlewareInterface $middleware): void {
        $this->middlewareChain = new MiddlewareContainer($middleware, $this->middlewareChain);
    }

    /**
     * All paths used within the handler must start with a slash (/) and not end with one.
     *
     * @param string $path
     *
     * @throws InternalServerException if invalid path is supplied
     */
    public static function validatePath(string $path): void {
        $path = trim($path);
        if (!str_starts_with($path, '/')) {
            throw new InternalServerException("Invalid path: $path: Must start with /");
        } elseif (str_ends_with($path, '/')) {
            throw new InternalServerException("Invalid path: $path: Must not end with /");
        }
    }

    public function isRpcPath(string $path): bool {
        if (empty($this->basePath)) {
            return true;
        }
        return mb_stripos($path, $this->basePath) === 0;
    }

    /**
     * @throws NotFoundException
     */
    public function rpcPath(string $path): string {
        if ($this->isRpcPath($path)) {
            if (empty($this->basePath)) {
                return $path;
            } else {
                return mb_substr($path, mb_strlen($this->basePath));
            }
        } else {
            throw new NotFoundException($path, "$path is not a rpc path (Must be under {$this->basePath})");
        }
    }

    /**
     * Process incoming OPTIONS requests (for CORS)
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     * @throws NotImplementedException
     * @throws BadRequestException
     */
    public function processOptions(ServerRequestInterface $request): ResponseInterface {
        if (CorsProcessor::isCorsPreflightRequest($request)) {
            if ($this->corsProcessor !== null) {
                return $this->corsProcessor->processPreflight($request);
            } else {
                throw new NotImplementedException("Received CORS preflight request, but no CORS processor set.");
            }
        } else {
            throw new BadRequestException("OPTIONS request received, but not a valid CORS Preflight request");
        }
    }

    public function addCorsResponseHeaders(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        if ($this->corsProcessor !== null) {
            return $this->corsProcessor->addResponseHeaders($request, $response);
        } else {
            return $response;
        }
    }

    public function validateCorsRequest(ServerRequestInterface $request): void {
        if ($this->corsProcessor !== null) {
            $this->corsProcessor->validateRequest($request);
        }
    }

    /**
     * @throws InternalServerException
     * @noinspection PhpRedundantCatchClauseInspection
     */
    public function process(ServerRequestInterface $request): ResponseInterface {
        try {
            try {
                $uri = $request->getUri();
                $rpcPath = $this->rpcPath($uri->getPath());
                $rpcUri = $uri->withPath($rpcPath);
                $rpcRequest = $request->withUri($rpcUri);
                $this->validateCorsRequest($request);
                if ($request->getMethod() == "OPTIONS") {
                    return $this->processOptions($request);
                }
                // Process GET/POST request:
                $maybeResponse = $this->middlewareChain->handleRequest($rpcRequest);
                if ($maybeResponse != null) {
                    // If CorsProcessor is set, add CORS headers on the response.
                    return $this->addCorsResponseHeaders($request, $maybeResponse);
                }
            } catch (UserfriendlyException $userfriendlyException) {
                // RPC handler has deliberately thrown a UserfriendlyException that should be returned to caller.
                $this->monitoring->reportUserfriendlyException($userfriendlyException);

                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->userfriendlyExceptionJsonResponse($userfriendlyException)
                );
            } catch (BadRequestException $badRequestException) {
                // RPC framework could not handle incoming request.
                $this->monitoring->reportBadRequestException($badRequestException);

                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->badRequestExceptionJsonResponse($badRequestException)
                );
            } catch (UnauthorizedException $unauthorizedException) {
                // Request handling aborted because of missing or invalid authentication
                $this->monitoring->reportUnauthorizedException($unauthorizedException);
                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->unauthorizedExceptionJsonResponse($unauthorizedException),
                );
            } catch (ForbiddenException $forbiddenException) {
                // Request handling aborted because authorization to perform rpc call was denied
                $this->monitoring->reportForbiddenException($forbiddenException);
                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->forbiddenExceptionJsonResponse($forbiddenException),
                );
            } catch (InternalServerException $internalServerException) {
                // RPC framework failed during it's processing.
                $this->monitoring->reportInternalServerException($internalServerException);

                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->internalServerErrorJsonResponse($internalServerException)
                );
            } catch (ServiceUnavailableException $serviceUnavailableException) {
                $this->monitoring->reportServiceUnavailableException($serviceUnavailableException);

                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->serviceUnavailableExceptionJsonResponse($serviceUnavailableException)
                );
            } catch (MethodNotAllowedException $methodNotAllowedException) {
                $this->monitoring->reportMethodNotAllowedException($methodNotAllowedException);

                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->methodNotAllowedExceptionJsonResponse($methodNotAllowedException)
                );
            } catch (NotImplementedException $notImplementedException) {
                $this->monitoring->reportNotImplementedException($notImplementedException);

                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->notImplementedExceptionJsonResponse($notImplementedException)
                );
            } catch (NotFoundException $notFoundException) {
                $this->monitoring->reportNotFoundException($notFoundException);

                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->notFoundResponse($request)
                );
            } catch (Throwable $other) {
                // Some other unhandled exception was thrown during RPC processing.
                $this->monitoring->reportOtherException($other);

                return $this->addCorsResponseHeaders(
                    $request,
                    $this->requestResponseUtils->internalServerErrorJsonResponse(
                        new InternalServerException("Unhandled exception from rpc handler", previous: $other)
                    )
                );
            }

            // No endpointClass matched the uri. Report 404 not found
            return $this->addCorsResponseHeaders($request, $this->requestResponseUtils->notFoundResponse($request));
        } catch (JsonException $jsonException) {
            $this->monitoring->reportOtherException($jsonException);

            return $this->addCorsResponseHeaders(
                $request,
                $this->requestResponseUtils->newJsonResponse(
                    '{"code":500,"message":"Json encoding failure"}',
                    Http::STATUS_CODE_INTERNAL_SERVER_ERROR
                )
            );
        }
    }
}
