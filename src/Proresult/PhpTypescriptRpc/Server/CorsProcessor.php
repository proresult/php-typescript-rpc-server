<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Server;

    use Exception;
    use JetBrains\PhpStorm\Pure;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\BadRequestException;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;

    /*
     * Since the rpc server often will handle requests on a separate (sub)domain from the webserver serving the pages that performs rpc requests, we need a CORS
     *  processor that allows the cross origin rpc requests.
     *
     * By plugging this into the RpcRouter, CORS preflight requests will be handled, and proper CORS headers will be added to responses.
     */
class CorsProcessor {
    public const ACCESS_CONTROL_MAX_AGE = '7200'; /* Default max value in chromium according to MDN */

    /**
     * @var String[]
     */
    private array $allowedRequestMethods;
    /**
     * @var AllowedCorsOrigin[]
     */
    private array $allowedCorsOrigins;

    private ResponseFactoryInterface $responseFactory;

    private bool $allowCookies;

    /**
     * CorsProcessor constructor.
     *
     * @param String[]            $allowedRequestMethods
     * @param AllowedCorsOrigin[] $allowedCorsOrigins
     * @param bool $allowCookies Set to true to return "Access-Control-Allow-Credentials: true" on requests, allowing the use of auth cookies.
     */
    public function __construct(array $allowedRequestMethods, array $allowedCorsOrigins, ResponseFactoryInterface $responseFactory, bool $allowCookies) {
        $this->allowedRequestMethods = $allowedRequestMethods;
        $this->allowedCorsOrigins    = $allowedCorsOrigins;
        $this->responseFactory       = $responseFactory;
        $this->allowCookies = $allowCookies;
    }

    public static function isCorsPreflightRequest(ServerRequestInterface $request): bool {
        return $request->hasHeader("Origin") &&
               $request->getMethod() === "OPTIONS" &&
               $request->hasHeader("Access-Control-Request-Method");
    }

    public function setAllowedCorsOrigins(AllowedCorsOrigin ...$allowed): self {
        $this->allowedCorsOrigins = $allowed;
        return $this;
    }

    public function isValidRequestMethod(string $requestMethod): bool {
        return in_array($requestMethod, $this->allowedRequestMethods);
    }

    #[Pure] public function isOriginAllowed(string $origin): bool {
        foreach ($this->allowedCorsOrigins as $allowedCorsOrigin) {
            if ($allowedCorsOrigin->match($origin)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param array<string> $arr
     *
     * @return string|null
     * @throws Exception
     */
    private static function singleValue(array $arr): ?string {
        if (empty($arr)) {
            return null;
        } elseif (count($arr) === 1) {
            return $arr[0];
        } else {
            throw new Exception("Too many values in array. Expected 1 element");
        }
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     * @throws BadRequestException
     * @noinspection PhpUnnecessaryLocalVariableInspection
     */
    public function processPreflight(ServerRequestInterface $request): ResponseInterface {
        // Process Access-Control-Request-Method
        $requestMethods = array_filter($request->getHeader("Access-Control-Request-Method"), fn(string $value) => !empty($value));
        if (count($requestMethods) > 1) {
            throw new BadRequestException("More than one \"Access-Control-Request-Method\" header in preflight request");
        }
        if (empty($requestMethods)) {
            throw new BadRequestException("No \"Access-Control-Request-Method\" value in preflight request header");
        }
        $requestMethod = $requestMethods[0];
        if (!$this->isValidRequestMethod($requestMethod)) {
            throw new BadRequestException("\"$requestMethod\" is not a valid \"Access-Control-Request-Method\" value in preflight request");
        }
        // Start building the success response
        $response = $this->responseFactory->newResponse(204, null, null);

        // Process Access-Control-Request-Header
        // We allow all request headers when the request otherwise is approved
        // XXX Should we have a separate "allowed cors headers" property that we add to the response here?
        $requestHeaders = array_filter(
            array_map(
                fn(string $v) => trim($v),
                explode(",", $request->getHeaderLine("Access-Control-Request-Headers"))
            ),
            fn(string $v) => !empty($v)
        );
        if (!empty($requestHeaders)) {
            $response = $response->withHeader("Access-Control-Allow-Headers", $requestHeaders);
        }

        // Let client cache the preflight result for better performance
        $response = $response->withHeader("Access-Control-Max-Age", self::ACCESS_CONTROL_MAX_AGE);

        // Add Access-Control-Allow-Origin
        $response = $this->addAccessControlAllowOrigin($request, $response);

        // Add Access-Control-Allow-Credentials header (for cookies)
        $response = $this->addAccessControlCredentials($response);

        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return void
     * @throws BadRequestException
     */
    public function validateRequest(ServerRequestInterface $request): void {
        try {
            $origin = self::singleValue($request->getHeader("Origin"));
        } catch (Exception $e) {
            throw new BadRequestException("Multiple Origin header values in incoming request", 0, $e);
        }
        if ($origin !== null && !$this->isOriginAllowed($origin)) {
            throw new BadRequestException("Origin \"{$origin}\" is not an allowed CORS origin");
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    private function addAccessControlAllowOrigin(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $origin = self::singleValue($request->getHeader("Origin"));
        if ($origin !== null && $this->isOriginAllowed($origin)) {
            $response = $response->withHeader("Access-Control-Allow-Origin", $origin);
        }
        // We always set the "Vary: Origin" response header to signal to the client that i could get a different response with a different Origin header value.
        $response = $response->withHeader("Vary", "Origin");
        // If no origin header value is received, the request is not a CORS request, so the response header is not set.
        return $response;
    }

    private function addAccessControlCredentials(ResponseInterface $response): ResponseInterface {
        if ($this->allowCookies) {
            return $response->withHeader("Access-Control-Allow-Credentials", "true");
        } else {
            return $response;
        }
    }

    public function addResponseHeaders(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $response = $this->addAccessControlAllowOrigin($request, $response);
        /* @noinspection PhpUnnecessaryLocalVariableInspection */
        $response = $this->addAccessControlCredentials($response);
        return $response;
    }
}
